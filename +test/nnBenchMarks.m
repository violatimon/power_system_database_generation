benchmark.acc = @(tp,tn,fp,fn) ((tp+tn)/(tp+tn+fp+fn)); % accuracy, ACC
benchmark.mcc = @(tp,tn,fp,fn) ((tp*tn-fp*fn)/(sqrt((tp+fp)*(tp+fn)*(tn+fp)*(tn+fn)))); % Mathews correlation coefficient
benchmark.tpr = @(tp,tn,fp,fn) (tp/(tp+fn)); % recall, sensitivity, hit rate, true positive rate, TPR
benchmark.tnr = @(tp,tn,fp,fn) (tn/(tn+fp)); % specificity, selectivity, true negative rate, TNR
benchmark.fpr = @(tp,tn,fp,fn) (fp/(fp+tn)); % falsePositiveRate, fall-out, FPR
benchmark.fdr = @(tp,tn,fp,fn) (fp/(fp+tp)); % falseDiscoveryRate, FDR
