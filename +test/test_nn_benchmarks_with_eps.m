% Evaluate effects of eps on NN benchmarks. Main reference: https://en.wikipedia.org/wiki/Confusion_matrix
epsArray = 0:0.1:10;


 %% ===== constants =====
hidden_layer = 50;
ReLU_layers = 3;
path = [pwd,filesep,'neural_network',filesep,'qLimitsNotEnforcedQS3_2relaxed',filesep]; % dynamic path to q lims not enforced, relaxed
%% -----  NN data  -----
[W_input,W,W_output,bias, NN_input, NN_output] = milp.get_nn_data(path);

%%
% load test data 
Y_test=csvread(strcat(path,'y_test.csv'));
X_test=csvread(strcat(path,'DB_test.csv'));

% load training data
Y_train=csvread(strcat(path,'y_train.csv'));
X_train=csvread(strcat(path,'DB_train.csv'));

Nr_samples_test = size(X_test,1);
Nr_samples_train = size(X_train,1);

% Benchmark definitions
test.nnBenchMarks

% pre-allocate results array
benchArr = cell(1,length(epsArray));
confArr = cell(1,length(epsArray));

%% eps loops
for idxEps = 1:length(epsArray)
    fprintf('%2.2f %%\n',(idxEps/length(epsArray))*100)
    pred_test = zeros(Nr_samples_test,2);
    pred_train = zeros(Nr_samples_train,2);

    % loop over test samples
    for i = 1:Nr_samples_test
        u_sample = X_test(i,:).';
        x_0 = W_input*(u_sample) + bias{1};
        x_0_ReLU = max(x_0,0);
        for j = 1:ReLU_layers-1
            x_0 = W{j}*x_0_ReLU + bias{j+1};
            x_0_ReLU = max(x_0,0);
        end
        %pred_test(i,:) = softmax(W_output*x_0_ReLU + bias{end});
        pred_test(i,:) = W_output*x_0_ReLU + bias{end};
    end

    % loop over train samples
    for i = 1:Nr_samples_train
        u_sample = X_train(i,:).';
        x_0 = W_input*(u_sample) + bias{1};
        x_0_ReLU = max(x_0,0);
        for j = 1:ReLU_layers-1
            x_0 = W{j}*x_0_ReLU + bias{j+1};
            x_0_ReLU = max(x_0,0);
        end
        pred_train(i,:) = softmax(W_output*x_0_ReLU + bias{end} );
        %pred_train(i,:) = W_output*x_0_ReLU + bias{end};
    end

    pred_test_1D    = pred_test(:,1) > (pred_test(:,2)+ epsArray(idxEps));
    pred_train_1D   = pred_train(:,1) > pred_train(:,2);
    labels_train_1D = Y_train(:,1) > Y_train(:,2);
    Y_test_1D       = Y_test(:,1) >= Y_test(:,2);
    
    [TP,TN,FP,FN] = getConfVal(pred_test_1D,Y_test_1D); % print these values?
    cnv = {TP,TN,FP,FN};
    
    benchArr{idxEps} = test.evalBenchmarks(benchmark,cnv,false);
    confArr{idxEps} = cnv;
end
%% plot ?
fNms = fieldnames(benchArr{1});
res = nan(length(fNms),length(epsArray));% rows -> benchmark, col -> eps
for idxEps = 1:length(epsArray)
    res(:,idxEps) = cellfun(@(x)(benchArr{idxEps}.(x)),fNms);
end
f = figure('Name','NN benchmarks');

for i = 1:length(fNms)
    sp = subplot(ceil(length(fNms)/2),2,i);
    plot(epsArray,res(i,:),'LineWidth',1.5)
    sp.YMinorGrid = 'on';
    ylabel(upper(fNms{i}))
    legend(upper(fNms{i}))
end
title({'NN benchmarks';'\epsilon = [0;100]'});
xlabel('\epsilon')
%%
function [TP,TN,FP,FN] = getConfVal(orig,pred)
    adder = orig + pred;
    TP = length(find(adder == 2));
    TN = length(find(adder == 0));
    subtr = orig - pred;
    FP = length(find(subtr == -1));
    FN = length(find(subtr == 1));
end