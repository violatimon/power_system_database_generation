close all
clear all

% define named indices into bus, gen, branch matrices
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, RATE_C, ...
    TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST, ...
    ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, ...
    MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, ...
    QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;


%load mpc file
% attention: if system has multiple generators at 1 bus this needs to be
% treated a-priori
%%
mpc= util.case14_wind;%case14
% ===== Relax Q limits
mpc.gen(:,QMAX) = mpc.gen(:,QMAX)+0.25*(mpc.gen(:,QMAX)-mpc.gen(:,QMIN));
mpc.gen(:,QMIN) = mpc.gen(:,QMIN)-0.25*(mpc.gen(:,QMAX)-mpc.gen(:,QMIN));
% ===== Relax S limits
sTol = 1.3;
mpc.branch(:,6) = mpc.branch(:,6)*sTol; 
mpc.branch(:,7) = mpc.branch(:,7)*sTol;
mpc.branch(:,8) = mpc.branch(:,7)*sTol;

mpc.bus(mpc.gen(:,GEN_BUS),VMAX) = mpc.gen(:,VG)+0.00001;
mpc.bus(mpc.gen(:,GEN_BUS),VMIN) = mpc.gen(:,VG)-0.00001; 
% ===== Increase Demand
%mpc.bus(:,PD) = mpc.bus(:,PD);%*1.09;

% list of line contingencies
con_list = mpc.contingencies; %[[5 7]];%%%

% indices of voltage set-points of generators
v_IDs = mpc.gen(:,GEN_BUS);

% generator indices 
ID_gen = find(mpc.gen(:,PMAX)-mpc.gen(:,PMIN)>=10^-4); %only look at generators
    
   
    
[maxP,id_maxP] = max(mpc.gen(:,PMAX)-mpc.gen(:,PMIN));
fprintf('setting slack bus to the largest generator: G%d\n',id_maxP);
mpc.bus(mpc.bus(:,2)==REF,2) = PV;
mpc.bus(mpc.gen(id_maxP,GEN_BUS),2) = REF;


[maxP2,id_maxP2] = max(mpc.gen(ID_gen,PMAX)-mpc.gen(ID_gen,PMIN));
% remove slack bus from ID_gen
% active power of generator compensates the 
ID_gen(id_maxP2) = [];
 
% build N-1 case, external function
mpc_N1 = test.build_N1(mpc,con_list,ID_gen,v_IDs);

opt_opf = mpoption;
opt_opf.opf.ac.solver='IPOPT';
results = runopf(mpc,opt_opf);
results_N1=runopf(mpc_N1,opt_opf);

% results = runopf(mpc);
% results_N1=runopf(mpc_N1);


% cost increase considering N-1 security (in percent)
cost_of_security = ((results_N1.f/results.f)-1)*100

                
% TODO: here, using your constraint validation pipeline, please
% double-check that the obtained solution does satisfy the N-1 security
% criterion
%%
mpcOpt = mpoption;
mpcOpt.verbose = 0;
mpcOpt.out.all = 0;
qOpt = 0;
mpcOpt.pf.enforce_q_lims = qOpt;

%mpc.contingencies = [5;7];

genList = util.GetGenList(mpc);

%sp = mpc.gen(boolean(genList),PG)';
sp = results_N1.gen(2:5,2)';
[classification,classDetails,dampingRatio,pfSuccess] = util.classifyCase(mpc,sp,genList,mpcOpt);
fprintf('=============================\n')
fprintf('1 - true; \t 0 - false \n')
fprintf('pf converged: \t\t\t%d\n',boolean(pfSuccess))
fprintf('ACOPF constraints fulfilled:\t%d\n',classification(1))
fprintf('Small Signal Stability     :\t%d\n',classification(2))
fprintf('   | P  |  Q |  Vm  |  S  | SSS | N_contingency\n')
disp(classDetails)

disp('damping ratios:')
disp(dampingRatio)
disp('set points OPF:')
disp(results.gen(2:5,2)')
disp('set points N-1 OPF:')
disp(sp)

fprintf('ACOPF price:\t\t%3.2f\n',results.f)
fprintf('N-1 ACOPF price:\t%3.2f\n',results_N1.f)
%results_N1.branch(:,[14,16])

