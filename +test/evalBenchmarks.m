function varargout = evalBenchmarks(bench,val,print)
tableW = 10;
format = "%-"+tableW+"s<strong>%f</strong>\n";
bNames = fieldnames(bench);
for i=1:length(bNames)
    if(print)
        if(i==1)
           fprintf('%s\n',repmat('-',1,2*tableW)) 
        end
       fprintf(format,upper(bNames{i}),bench.(bNames{i})(val{:}));
    end
    if nargout
       outStruct.(bNames{i}) = bench.(bNames{i})(val{:});
    end
end
if nargout
   varargout{1} = outStruct;
end
end