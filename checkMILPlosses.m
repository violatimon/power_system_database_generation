MILP_with_recur_losses
%%
P_demand_pu = P_demand_i;
Losses_pu = - l(1)...
    - l(2)*(u_NN(1)*optvar2pu(1) - pGeneration(2)) ...
    - l(3)*(u_NN(2)*optvar2pu(2) - pGeneration(3))...
    - l(4)*(u_NN(3)*optvar2pu(3) - pGeneration(4))...
    - l(5)*(u_NN(4)*optvar2pu(4) - pGeneration(5));
Losses_pu = value(Losses_pu);

milp_full_generation = (value(Pslack)*mpc.baseMVA + sum(setpoints));
mpc_ = mpc;
fprintf('Total system real losses(MILP): %3.2f\n',abs(Losses_pu*mpc.baseMVA))
%% Run matpower (forced to MILP solution values)
% setpoints - milp solution
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;

genList = util.GetGenList(mpc);
% acopf full generation - acopf demand = acopff losses
tol = 1;
mpc.gen(genList, PMIN) = setpoints' -tol;
mpc.gen(genList, PMAX) = setpoints' +tol;
[results,~] = runopf(mpc,mpcOpt);
matpowerLosses = get_losses(results);

fprintf('Total system real losses(OPF) : %3.2f\n',sum(real(matpowerLosses)))
setpointsACOPF = results.var.val.Pg(2:end) * results.baseMVA;

%%
mpc = mpc_;
mpc.gen(genList, PG) = setpoints';
[results,~] = runpf(mpc,mpcOpt);
matpowerLosses = get_losses(results);

fprintf('Total system real losses(PF)  : %3.2f\n',sum(real(matpowerLosses)))
