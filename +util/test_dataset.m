% Generate a comprehensive analysis of the dataset generated by
% 'gen_dataset_bf*.m
% Consider running 'unify_datasets.m' before running this script to obtain
% the plot of the balanced (enriched) data-set.
%
util.unify_q_s3_relaxed_dataset
genReport = false;

idxStablePoints = ((classification(:,2) == 1) & (classification(:,3) == 1));

fprintf('Number of stable points: %d\n',sum(idxStablePoints))
fprintf('Dataset ratio:\t \t %4.3f%%\n',100 * sum(idxStablePoints)/length(idxStablePoints))
[stablePointNo,~] = find(idxStablePoints);

%% which points are close to feasible
% feasible = 95
feasible = (size(classDetails{1},1))*(size(classDetails{1},2)-1); % automatically sets feasibility limit based on number of contingencies considered
n = length(classDetails);
x = 1:n;
y = nan(n,1);
for i = 1:n
    y(i) = sum(sum(classDetails{i}(:,1:5)));
end
c =[ 3.2*(ones(n,1) - y/feasible),y/feasible, zeros(n,1) ];

figure('Name','Distance from feasibility');
axes;
hold on
scatter(x,y,5,c) %{:}(:,1:5))
plot(gca,[100,n],[feasible,feasible],'color','#EDB120','LineStyle',':','LineWidth',2)
text(n/2,feasible+2,'Feasibility','color','k')
ylim([50,100])
xlabel('Data index')
ylabel('Distance from feasibility')

%% which points are close to feasible - P set sum
x_sp = nan(n,1);
for i = 1:n
    x_sp(i) = sum(setpoints(i,:));
end
fig = figure('Name','Distance from feasibility [setpoints]');
ax1 = axes(fig,'Position',[.1 .15 .9 .7]);
hold on
title('N-1 feasability vs setpoint sum')
scatter(x_sp,y,5,y) %{:}(:,1:5))
colormap(jet)
cb = colorbar;
plot(gca,ax1.XLim,[feasible,feasible],'color','#EDB120','LineStyle',':','LineWidth',2)
text(mean(ax1.XLim),ax1.YLim(2)+2,'Feasibility','color','k')
ylim([ax1.YLim(1)-10,ax1.YLim(2)+10])
xlabel('Sum P_{G2-G5}')
yl = ylabel('Distance from feasibility');
ax1.XGrid = 'on';
ax1.YGrid = 'on';
t1 = text(ax1.XLim(1)+5,ax1.YLim(1)+7,['no. stable points: ',num2str(sum(idxStablePoints))]);
t2 = text(ax1.XLim(1)+5,ax1.YLim(1)+3,['Dataset ratio:      ',num2str(100 * sum(idxStablePoints)/length(idxStablePoints)),'%']);


ax2 = axes(fig,'Position',[0.1,ax1.Position(2)+ax1.Position(4),ax1.Position(3),0.1]);%,GRoup,GCount,'Position',[.7 .7 .2 .2]);
[GCount,GRoup]=groupcounts(x_sp);
bh=bar(ax2,GRoup,GCount);
ax2.XAxis.Visible = 'off';
ax2.YAxis.Visible = 'off';
bh.EdgeColor = 'b';
bh.BaseLine.Visible = 'off';
linkaxes([ax1 ax2],'x')
t = title('N-1 feasibility vs. setpoint sum');
%t.Position = 1.0e+04 * [0.0131,4.3878,0];
t.FontSize = 12;

fig.Position = [398 286 426 364];
cb.Position = [0.8903 0.1500 0.0427 0.5963];
ax1.Position = [0.1000 0.1500 0.7719 0.6988];
yl.Position = [-13.3537   82.5000   -1.0000];
% Note: To export the figure in proper quality go to File>Export Setup>Load
% > and load myDataSet profile. This profile was set up for this figure.

%% Wich stability check fail how many times
constraintTypes = {'P','Q','V','S','SSS'};
nCont = length(classDetails{1});
checks = zeros(1,5);
for i = 1:n
    checks = checks + sum(classDetails{i}(:,1:5));
end
figure('Name','AC opf and sss constraints')
axes;
bar(checks);
hold on
title({'Constraint feasibility by type'; ['Max= ',num2str(nCont*n)]})
ylabel('no. success')
xlabel('type')
plot(gca,[0,6],[nCont*n,nCont*n],'r:','LineWidth',2)
set(gca,'xticklabel', constraintTypes)

%% Where do we have high/low feasability checks
colors={[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],...
    [0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880]};
figure('Name','Constraint feasability in eventspace')
constraintPass = zeros(n,5);
for i = 1:n
    constraintPass(i,:) = sum(classDetails{i}(:,1:5));
end
constraintPass = constraintPass/nCont; % normalization
axes;
hold on
opt = {'filled','MarkerFaceAlpha',.5};
for i = 1:5
    subplot(2,3,i)
    scatter(x,constraintPass(:,i),15,colors{i},opt{:},'DisplayName',constraintTypes{i});
    hold on
    plot([x(1) x(end)],[1,1],'r:','LineWidth',2)
    title(constraintTypes{i})
    %     scatter(x,constraintPass(:,2),opt{:})
%     scatter(x,constraintPass(:,3),opt{:})
%     scatter(x,constraintPass(:,4),opt{:})
%     scatter(x,constraintPass(:,5),opt{:})
    ylim([-0.1 1.1])
end


figure('Name','Constraint feasability all')
axes;
hold on;
scatter(x,constraintPass(:,1),opt{:});
scatter(x,constraintPass(:,2),opt{:})
scatter(x,constraintPass(:,3),opt{:})
scatter(x,constraintPass(:,4),opt{:})
scatter(x,constraintPass(:,5),opt{:})
ylim([-0.1 1.1])

legend(constraintTypes,'Location','southoutside','NumColumns',5,'box','on')

%% contingency failure
figure('Name', 'Contingency failure')
branchNo = classDetails{1,1}(:,end)';
noContSuccess = zeros(1,nCont);
for i = 1:n
   noContSuccess = noContSuccess + all(classDetails{i}(:,1:5)');
end

bar(noContSuccess)
title('contingency failures by branch')
ylabel('number of passes')
xlabel('branch')
try
   
   % disp('falling back to default mpc: util.case14_wind')
    branch = ["intact", string(classDetails{1}(2:end,end)')];
catch
    branch = repmat('-',nCont,1);
end
set(gca,'xtick',1:nCont)
set(gca,'xticklabel',branch)

set(gca,'XTickLabelRotation',45) 
%% ----- damping ratio -----
if exist('dampingRatio','var')
    figure('Name','Damping ratio');
    axes;
    hold on
    scatter(x,dampingRatio,5,c)
    plot(gca,[100,n],[0.03,0.03],'color','#EDB120','LineStyle',':','LineWidth',2)
    text(n/2,0.04,'Feasibility','color','k')
    %ylim([50,100])
    xlabel('Data index')
    ylabel('Distance from feasibility')
    dr = dampingRatio(idxStablePoints);
    fprintf('Damping ratios:\n')
    disp(dr(1:10))
end

%% ----- generate report -----
if genReport
    prompt = 'Report title: ';
    str = input(prompt,'s');
if isempty(str)
    str = "test_dataset-[" + string(datestr(now)) + "]";
end
    report.title = str;
    report.fName = str;
    report.subTitle = str;
    util.pptgen(report)
    
end