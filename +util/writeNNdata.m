% Create csv NN training data
%
% Neural Network input data are the generator setpoints, this data is
% written to NNinput.csv
% NN output data are two columns. Col 1 is the over feasability
% classification (col 1 of classification from <<gen_dataset_bf.m>>). The
% second column is the logical inverse of the first. (This is a technical
% caveat that enhances the classification accuracy of the trained network.)

% 
% load('/home/timon/Documents/powersys/power_system_database_generation/database_files/case14db_qLims2__11_03_1813.mat')
% cla2 = classification;
% sp2 = setpoints;
% load('/home/timon/Documents/powersys/power_system_database_generation/database_files/case14db_qLims2__10_20_1915.mat')
% class = [classification; cla2];
% setp = [setpoints; sp2];
% classification = class;
% setpoints = setp;
% nnclass = [classification(:,1), ~classification(:,1)];

% normalization
genRange = [60 60 25 25]; % from mpc
setpoints = setpoints./genRange;


writematrix(setpoints,'NN_input.csv')
writematrix(nnclass,'NN_output.csv')
