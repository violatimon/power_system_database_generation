function [class, varargout] = classifyCase(mpc,setpoints,genList,mpOption)
% Returns the classification of the power flow case (defined by mpc). 
% [classification,classDetails,dampingRatios,pfSuccess] = CLASSIFYCASE(mpc,setpoints,genList,mpOption);
% where mpc - power flow case given in MATPOWER format, the case has to
%       have a 'contingencies' field which defines which contingencies to
%       check in the N-1 scenarios
%       setpoints - the generator setpoints of the case that shall be
%       checked
%       genList - the list of generators that can be modified. Use
%       GETGENLIST
%       mpOption - MATPOWER mpoption struct
% outputs:
%       classification(1) - ACOPF constraints fulfilled if 1.
%       classification(2) - Small Signal Stabilitty fulfilled if 1.
% (opt) classDetails - N-1 case information with respect to the
%       'contingencies' field of the mpc. Each row corresponds to a
%       contingency, the columns refer to the checks in the following
%       order: | P | Q | Vm | S | SSS | N_contingency|.
% (opt) dampingRatios - the smallest value of the damping ratios for all
%       cases (intact and N-1).
% (opt) pfSuccess - 1 if runpf converged for all cases (intact and N-1).
%

    import util.checkLims
    BR_STATUS = 11;
    PG = 2;
    class = [0,0]; % [AC-OPF constraints, small sig stab]
    
    nb = length(mpc.branch(:,1));
    cont = mpc.contingencies;
    mpc.branch(:, BR_STATUS) = ones(nb,1);  % intact case
    brStat = ones(nb,1);
    pfsuccess = nan(length(cont)+1,1);
    classDetails = nan(length(cont)+1, 6); % p/q/v/s/DR cheks + cont no.
    dampingR = nan(length(cont)+1, 1);
    % Set generator set points w/ the right multipliers
    mpc.gen(genList, PG) = setpoints';
        
    % Intact case pf
    [results, pfsuccess(1)] = runpf(mpc,mpOption); % intact case
%     if SUCCESS ~= 1
%         return
%     end
    
    % Check intact case
    [class,classDetails(1,1:5),dampingR(1)] = checkLims(results, brStat);
    classDetails(1,6) = 0;
%     if sum(class) ~= 2
%         class(1) = 0; % ac-opf constraints for n-1 cases not checked
%         return
%     end
    
    % N-1 cases
    for nm = 1:length(cont) % we only check for specific branches
        brStat(cont(nm)) = 0;
        mpc.branch(:,BR_STATUS) = brStat;
        
        [results, pfsuccess(nm+1)] = runpf(mpc,mpOption);
%         if SUCCESS ~= 1
%             break
%         end
        
        [class,classDetails(nm+1,1:5),dampingR(nm+1)] = checkLims(results, brStat);
        classDetails(nm+1, 6) = cont(nm);
        
%         if sum(class) ~= 2 % break immediately if any of the points unstable
%             break
%         end
    
        brStat = ones(nb,1);
    end
    % everything passed, the setpoint is AC-OPF and
    % small signal stable
    class(1) = all(classDetails(:,1:4),'all');
    class(2) = all(classDetails(:,5), 'all');
%         class(1) = sum(classDetails(1:4)) < length(cont) * 4;
%     class(2) = sum(classDetails(5)) < length(cont);
    if nargout == 2
        varargout{1} = classDetails;
    elseif nargout == 3 
        varargout{1} = classDetails;
        varargout{2} = min(dampingR);
    elseif nargout ==4
        varargout{1} = classDetails;
        varargout{2} = min(dampingR);
        varargout{3} = ~any(0 == pfsuccess);
    end
    
end
