function pptgen(varargin)
%PPTPOP PPT-based report example
%   REFERENCE: openExample('rptgen/population_slides')
%
%   This example is based on the MATLAB example: "Predicting the US
%   Population." That example was generated using MATLAB Publish. This
%   example is generated using the PPT API. This example is intended to
%   illustrate a simple use of the API.
%
%   pptpop() generates a MS PowerPoint presentation.

% This statement eliminates the need to qualify the names of PPT
% objects in this function, e.g., you can refer to 
% mlreportgen.ppt.Presentation simply as Presentation.
import mlreportgen.ppt.*;

%% Create a presentation.

% Use the default template
if (nargin <1)
 pre = Presentation('autoReport');
elseif nargin == 1
    pre = Presentation(varargin{1}.fName);
end

%% Add the first slide to the presentation, based on the Title Slide layout.
% In general, PowerPoint presentations are structured in slides created 
% from predefined layouts that contains place holders which users will fill 
% in with generated content. The predefined layouts belong to a template 
% slide master that defines the styles.
slide = add(pre, 'Title Slide');

% Replace the title and subtitle in the slide, using the replace method 
% providing the placeholder name
replace(slide, 'Title', varargin{1}.title);
replace(slide, 'Subtitle', varargin{1}.subTitle);

%% All open figures are going to be printed to file
figHandles = findobj('Type', 'figure');
%% Add contents slide
contentsStr = {};
j= 1;
for i=length(figHandles):-1:1
    contentsStr{j} = figHandles(i).Name;
    j = j+1;
end

slide = add(pre,'Title and Content');
para = Paragraph('Contents');
para.FontColor = 'blue';
replace(slide,'Title',para);
 
replace(slide,'Content',contentsStr);
%% Add rest of the slides 


% Create a cell array to hold images generate for this presentation. 
% That will facilitate deleting the images at the end of presentation
% generation when they are no longer needed. 
images = {};

for i=length(figHandles):-1:1   % Iterate backwards
    set(0, 'CurrentFigure', figHandles(i)) % Set gcf to currently processed fig
    title = figHandles(i).Name;
    subTitle = figHandles(i).Children(1).Title.String;
    slide = add(pre, 'Title and Content');
    replace(slide, 'Title', title);
    replace(slide, 'Subtitle', subTitle);
    img = printPlot(title); % deletes gcf
    % Replace the Content placeholder by the plot
    replace(slide, 'Content', Picture(img));
    % Add plot image to the list of images to be deleted at the end of
    % presentation generation. Note that you need to keep the images around until
    % the presentation is closed. That is because you cannot add images to the
    % presentation package while the presentation file is still open.
    images = [images {img}]; %#ok
end



% %% Add the second slide to the presentation based on Title and Content layout.
% slide = add(pre, 'Title and Content');
% replace(slide, 'Title', 'Population Modeling Approach');
% % Add some bullets text to the Content placeholder, using a cell array
% replace(slide, 'Content', { ...
%     'Fit polynomial to U.S. Census data' ...
% 	'Use polynomials to extrapolate population growth' ...
% 	'Based on "Computer Methods for Mathematical Computations", by Forsythe, Malcolm and Moler, published by Prentice-Hall in 1977' ...
% 	'Varying polynomial degree shows riskiness of approach'});

%% Finally, close the presentation and open it in Windows
close(pre);
if ispc
    winopen(pre.OutputPath);
end

% Closing the presentation causes the images needed for the
% presentation to be copied into it. So we can now delete them.
for i = 1:length(images)
    delete(images{i});
end

end

function imgname = printPlot(name) %TODO: save picture into a .tmp folder
% Convert the specified plot to an image.
% Return the image name so it can be deleted at the
% end of presentation generation.
import mlreportgen.ppt.*;

% Select an appropriate image type, depending
% on the platform.
if ~ispc
    imgtype = '-dpng';
    imgname= [name '.png'];
else
    % This Microsoft-specific vector graphics format
    % can yield better quality images in Word documents.
    imgtype = '-dmeta';
    imgname = [name '.emf'];
end

% Convert figure to the specified image type.
print(imgtype, imgname);

% Delete plot figure window.
delete(gcf);

end


