%load('database_files/MILP_PRICE_CHANGES_QS3_25_02_20T22_29_13.mat')
load('MILP_PRICE_CHANGES_QS3_04_03_20T21_05_23')
%
if ~exist('epsArray','var')
    error('Could not find multiple eps values.You might have loaded the wrong file.')
end
fprintf('\nResults of %d simulations.\n',length(price))
fprintf('Eps values: ')
fprintf('%2.2f  ',epsArray)
fprintf('\n')
fprintf('<strong>|  eps\t | min DR | max DR | avg cost  | stability (%%)</strong>\n')
lenEps = length(epsArray);
shareOfStable = nan(1,lenEps);
avgCost = nan(1,lenEps);
for i = 1:lenEps
   vectCost = price(i:lenEps:end);
   vectDR = dampingRatio(i:lenEps:end);
   vectStab = stability(i:lenEps:end);
   stab_share = 100*sum(vectStab)/length(vectStab);
   fprintf('|  %2.2f  |  %2.2f  |  %2.2f  |  %2.2f  |  %2.2f  |\n',epsArray(i),...
       min(vectDR),max(vectDR),mean(vectCost),stab_share)
   shareOfStable(i) = stab_share;
   avgCost(i) = mean(vectCost);
end

f=figure('Name','Share of stable points');
ax = axes;
xlabel('\epsilon value','interpreter','tex')
yyaxis left
plot(epsArray,shareOfStable,'k--','Marker','^','LineWidth',1.5)
ylabel('share of stability %')
ax.YLim = [ 0.95*ax.YLim(1), 1.05*ax.YLim(2)];

yyaxis right
plot(epsArray,avgCost,'k:','Marker','o','LineWidth',1.5)
ylabel('average cost')

grid on

ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';
ax.GridColor = 'k';
ax.XTick = epsArray;
ax.XLim = [min(epsArray)-0.5,max(epsArray)+0.5];
legend({'share of stable instances';'avg cost of solution'},'Location','NorthWest')
ax.XMinorGrid = 'on';

%%
% always select 1 (ACOPF,N1SCACOPF,MILP) and select (9) (MILP+eps=8)
l = size(spaceGrid,1);
priceMat = nan(l,4);
timeMat = nan(l,4);
stabMat = nan(l,4);
MILP = 1;
AC = 2;
N1 = 3;

j = 1;
for i = 1:lenEps:length(price)
    priceMat(j,1) = resultStruct{i,AC}.price;
    priceMat(j,2) = resultStruct{i,N1}.price;
    priceMat(j,3) = resultStruct{i,MILP}.price;
    timeMat(j,1) = resultStruct{i,AC}.time;
    timeMat(j,2) = resultStruct{i,N1}.time;
    timeMat(j,3) = resultStruct{i,MILP}.time;
    stabMat(j,1) = resultStruct{i,AC}.stability;
    stabMat(j,2) = resultStruct{i,N1}.stability;
    stabMat(j,3) = resultStruct{i,MILP}.stability;
    j = j+1;
end
j =1;
for i = 9:lenEps:length(price)
    priceMat(j,4) = resultStruct{i,MILP}.price;
    timeMat(j,4) = resultStruct{i,MILP}.time;
    stabMat(j,4) = resultStruct{i,MILP}.stability;
    j = j+1;
end
vPr = mean(priceMat);
vTi = mean(timeMat);
vSt = (sum(stabMat,1)./l)*100;

eps = 8;
fprintf('Average of %d simulations.\n',size(spaceGrid,1))
simNms = {'ACOPF','N-1 SC ACOPF','MILP','MILP+eps'};
chr = fprintf('<strong>%-12s | cost of Sec  |  time  | stability (%%)</strong>\n',' ');

for i = 1:size(simNms,2)
    fprintf('<strong>%-12s</strong> | %3.2f\t\t| %3.2f s |  %2.2f\t%%\n',simNms{i},vPr(i),vTi(i),vSt(i))
end

getLossEstTimes()

function getLossEstTimes()
fName = 'log/MILP_PRICE_CHANGES_QS3_04_03_20T21_05_23.log';
fileID = fopen(fName,'r');
fSpec = '%s'; % as string
in = fscanf(fileID,fSpec); % apparently matlab cannot match regexp in file read...
fclose(fileID);

% Skip initial toc and final toc
in = in(250:end-50);

regExpre = '(?<=timeis)[0-9]*.[0-9]*'; % positive look behind assertion in order to extract the time stamp values
[C,matches] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
% process matches into numeric
timeStamps = str2double(matches);

% assume every timestamp entry belongs to the same operation => I can
% simply average


%% Get number or [0] iteration errors
regExpre = '\[0\]'; % positive look behind assertion in order to extract the time stamp values
[Ce0,matches_e0] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
err_0 = str2double(matches_e0);

regExpre = '\[1\]'; % positive look behind assertion in order to extract the time stamp values
[Ce1,matches_e1] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
err_1 = str2double(matches_e1);
%%
fprintf('Ratio of 2nd error estimation iteration: %3.2f %%\n',(size(matches_e1,2)/size(matches_e0,2))*100)
fprintf('Average loss estimation time: %3.2f sec\n',mean(timeStamps)) % averaging on all iterations
fprintf('Average solution time: %3.2f sec\n',sum(timeStamps)/size(matches_e0,2)) % accounting for iterations
fprintf('Average MILP time: %3.2f sec\n',mean(timeStamps(1:11:end))) % eps 0
nTimeStamps = timeStamps;
nTimeStamps(1:11:end) = [];
fprintf('Average MILP+eps time: %3.2f sec\n',mean(nTimeStamps))
end