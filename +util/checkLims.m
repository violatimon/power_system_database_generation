function varargout = checkLims(pfresults, status)
% Check 5 most critical contingencies: Pg,Qg,Vbus,S and SSS (small-signal
% stability.
% Inspired by MATPOWER CHECKLIMITS utility function:
% https://matpower.org/docs/ref/matpower7.0/extras/misc/checklimits.html

% init constants
PF = 14; QF=15; PT=16; QT=17; PQ = 1; PV = 2; PD = 3; QD = 4; VM = 8;
VMAX = 12; VMIN = 13; PMAX = 9; PMIN = 10; QMAX = 4; QMIN = 5; PG = 2; RATE_A = 6; RATE_B = 7; RATE_C = 8; VA = 9;
QG = 3; BUS_TYPE = 2;
tol = 1e-1;
success = zeros(1,5); % boolean vector to store checks' results

%% -----  generator real power  -----
pChecks = (pfresults.gen(:,PMIN)-tol <= pfresults.gen(:,PG)) & (pfresults.gen(:,PG) <= pfresults.gen(:,PMAX)+tol);
success(1) = ~any(0 == pChecks);

%% -----  generator reactive power  -----
qChecks = (pfresults.gen(:,QMIN)-tol <= pfresults.gen(:,QG)) & (pfresults.gen(:,QG) <= pfresults.gen(:,QMAX)+tol);
success(2) = ~any(0 == qChecks);

%% ----- bus voltage magnitude  -----
vmChecks = (pfresults.bus(:,VMIN)-tol <= pfresults.bus(:,VM)) & (pfresults.bus(:,VM) <= pfresults.bus(:,VMAX)+tol);
success(3) = ~any(0 == vmChecks);

%% -----  branch flows  -----
Ff = sqrt(pfresults.branch(:, PF).^2 + pfresults.branch(:, QF).^2);
Ft = sqrt(pfresults.branch(:, PT).^2 + pfresults.branch(:, QT).^2);
F = max(Ff, Ft);
% find branch flow violations
Fv(1) = any(find(F > pfresults.branch(:, RATE_A) + tol & pfresults.branch(:, RATE_A) > 0));
Fv(2) = any(find(F > pfresults.branch(:, RATE_B) + tol & pfresults.branch(:, RATE_B) > 0));
Fv(3) = any(find(F > pfresults.branch(:, RATE_C) + tol & pfresults.branch(:, RATE_C) > 0));

success(4) = sum(Fv) < 1;

%% ----- small-signal stability check -----
% damping ratio
OP.P = pfresults.gen(:,PG);
OP.V = pfresults.bus(:,VM);
OP.A = pfresults.bus(:,VA);
OP.Q = pfresults.gen(:,QG);
OP.PF = pfresults.branch(:,PF);
OP.PT = pfresults.branch(:,PT);
OP.QF = pfresults.branch(:,QF);
OP.QT = pfresults.branch(:,QT);
s_load = [pfresults.bus(:,PD) pfresults.bus(:,QD)];
bus_type = pfresults.bus(:,BUS_TYPE);

try
    [damping_ratio, ~, ~] = util.CheckOP(OP, s_load, status, bus_type);
catch err
    % disp(err);
    damping_ratio = inf;
end

% always select the min damping ratio
% damping_ratio_ = min(damping_ratio,damping_ratio_); % as far as I
success(5) = damping_ratio >= 0.03;%(0 <= damping_ratio) && (damping_ratio<= 0.03); % smaller than 3%
SUCCESS = [~any( 0 == success(1:4)), success(5)];

switch nargout
    case 1
        varargout{1} = SUCCESS;
    case 2
        varargout{1} = SUCCESS;
        varargout{2} = success;
    case 3
        varargout{1} = SUCCESS;
        varargout{2} = success;
        varargout{3} = damping_ratio;
    otherwise
    error('Too many output arguments')
end