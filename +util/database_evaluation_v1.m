% For a given point with a given classification compute the classification
% again to be sure how to use the function

clear all %#ok
close all
addpath('Florian small signal stability files\')
%% Load data
% load acopf data (presumably by running MATPOWER runopf on the case file
load(strcat(pwd,'\Database OPF with VG\acopf_op.mat')); % acopf_op
load(strcat(pwd, '\Database OPF with VG\indices.mat')); % indices
acopf_op_wVG=acopf_op;

% MATPOWER case file
mpc = case14_wind;

%% Define variables, options
% Define named indices into bus, gen, branch matrices - MATPOWER
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
 VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, RATE_C, ...
  TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST, ...
  ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, ...
 MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, ...
 QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;

pmax = mpc.gen(2:5,PMAX);

classification = all(acopf_op(idx_static,:), 1);
%sum(classification)/size(classification,2); % this was surpressed
input = acopf_op(2:5,:)./pmax;

% matpower options
option_pf = mpoption;
option_pf.out.all = 0;

% naming conventions for NN/classifier 
Input = input;
Output = classification;
Output_ = [Output; 1-Output];

% sizes
ng = size(mpc.gen, 1);% number of generators
nb = size(mpc.bus, 1);% number of buses
nl = size(mpc.branch, 1);% munber of lines
mapg2b = zeros(ng, nb);                         % map bus to generator???

for g = 1:ng
    mapg2b(g, mpc.gen(g,1)) = 1;                % set gen to max (1 pu)
end

mpc_init = mpc;

%% Sampling of ???
%sample to be checked
% the overall goal is to calculate the damping ratios
nSamp = 100;
damping_ratio_ref = nan(1,nSamp); % pre-allocate result vectors
damping_ratio_new = nan(1,nSamp);

for i = 1:nSamp
    mpc = mpc_init; % mpc will be mutated
 
    s_load = [mpc.bus(:,PD) mpc.bus(:,QD)];
    status = ones(nl,1);
    bus_type = mpc.bus(:,BUS_TYPE);
    
    % We need to ultimately check for every contingency
    %   checks only for the ones that the paper was talking about
    %   does not check for 0 contingency case
    br_IDs = [1,2,3,4,5,6,7,8,9,10,11,12,15,16,17,18,19,20]';
        % list of considered line fault inidces
    damping_ratio_ = 1000;
    
    for col = 2:19                          % we go through the columns why not all columns ?????
        status = ones(20,1);                % not sure why we redifine it with hardcoded values?
        status(br_IDs(col-1)) = 0;          % WHAT?????
        mpc_c = mpc;
        
        mpc_c.branch(br_IDs(col-1),:) = []; % the given branch falls out
        mpc_c.gen(:,PG) = acopf_op(idx_p,i);% Pg value from opf
        
        mpc_c.gen(:,VG) = mapg2b*acopf_op(idx_v,i); 
        %               ng*nb . nb
        % Generator value from ????
        
        % run MATPOWER powerflow for the mutated mpc
        results_pf = runpf(mpc_c,option_pf);
        
        % build OP struct for CHECKOP
        OP.P = results_pf.gen(:,PG);
        OP.V = results_pf.bus(:,VM);
        OP.A = results_pf.bus(:,VA);
        OP.Q = results_pf.gen(:,QG);
        OP.PF = results_pf.branch(:,PF);
        OP.PT = results_pf.branch(:,PT);
        OP.QF = results_pf.branch(:,QF);
        OP.QT = results_pf.branch(:,QT);
        
        [damping_ratio, violate_avr_limit, violate_volt_limit] =...
            CheckOP(OP, s_load, status, bus_type);
        
        damping_ratio;
        % always select the min damping ratio
        damping_ratio_ = min(damping_ratio,damping_ratio_); % as far as I
            %   understood, the smallest value is the most important,
            %   because that is the dominant mode in the system
    end
    
   acopf_op(idx_static,i)
   damping_ratio_ref(i) = acopf_op(idx_damp,i);
   damping_ratio_new(i)= damping_ratio_;
end
   
   fprintf('There is a difference between the reference and the new damping\nratios. Which raises the suspicion that the database is wrongly\n classified\n')
   disp('damping_ratio_ref')
   disp(damping_ratio_ref(1:10))
   disp('damping_ratio_new')
   disp(damping_ratio_new(1:10))
   
   rmpath('Florian small signal stability files\')

% CHECKOP help [DE]
%
%  und benoetigt den folgenden input:
%
%   OP - operating point
%       OP ist ein struct, das den operating point enthaelt, der zu testen 
%       ist und muss die folgenden Resultate enthalten: 
%           OP.P active power generation (MW),
%           OP.Q reactive power generation (Mvar),
%           OP.V voltage magnitudes (p.u.),
%           OP.A voltage angles (deg);
%
%   Load - mpc.bus(:,PD) (aendert sich fuer dich nicht; hat Florian nur 
%       verwedent, als er Datenbanken fuer verschiedene load levels generiert hat);
%   
%   status - line status 
%       Wenn keine contingencies beruecksichtigt werden, dann ist 
%       status = ones(number_of_lines,1), sonst enthaelt der Vektor eine Null
%       an der Stelle der ausser Betrieb gesetzten Leitung.
% 
%   bus_type - mpc.bus(:,BUS_TYPE) (aendert sich fuer dich auch nicht).
%
% CheckOP Output aus:
%     damping_ratio - Minimum damping ratio des OP (i.e., die damping ratio
%       die der "least damped mode" entspricht);
%
%     violate_avr_limit - 1 if AVR limits are violated, 0 otherwise;
%
%     violate_volt_limit - Florian ueberprueft in der File noch, ob gewisse
%       static limits eingehalten sind, aber ich wuerde dir empfehlen, die
%       static limits einfach selbst vor der Initialisierung zu ueberpruefen;
%       die static limits sind im OPF ohnehin eingehalten - es kann nur sein,
%       dass das Resultat vom OPF nicht N-1 secure sind und dann waeren im
%       Falle von contingencies die static limits verletzt. Ich wuerde CheckOP
%       nur zur Ueberpruefung der small-signal stability verwenden.
%
    
    
