function varargout = compareResults(mpc,mpOption,obj,setpoints,priceVec, mode)
% COMPARERESULTS runs ACOPF N-1SCACOPF to compare the results of the MILP
mode = ~strcmp(mode,'silent');

if mode
    chr = fprintf('|  MILP objective value:     <strong>%3.2f $/MW </strong> |\n',value(obj));
    chr = chr -17;
    fprintf(strcat('|', repmat('-',1,chr-3),'|\n'))
end
mpc.gencost(:,6) = priceVec';
genList = util.GetGenList(mpc);
[milpClass,milpDetails,milpDR,pfSuccess] = util.classifyCase(mpc,setpoints,genList,mpOption);
if mode
    pretty_print(pfSuccess,milpClass,chr)
end
milpST = pfSuccess && milpClass(1) && milpClass(2);

% -----      ACOPF    -----
tic;
[results,~] = runopf(mpc,mpOption);
opfTime = toc;

setpointsACOPF = results.var.val.Pg(2:end) * results.baseMVA;
[acopfClass,acopfDetails,acopfDR,pfSuccess] = util.classifyCase(mpc,setpointsACOPF,genList,mpOption);
acopfST = pfSuccess && acopfClass(1) && acopfClass(2);

if mode
    fprintf('|  ACOPF objective value:   <strong> %3.2f $/MW </strong> |\n',results.f);
    fprintf(strcat('|', repmat('-',1,chr-3),'|\n'))
    pretty_print(pfSuccess,acopfClass,chr)
end

% -----    N-1 SC ACOPF    -----
tic;
results_N1 = util.runn1scacopf(mpc,mpOption);
n1opfTime = toc;

sp = results_N1.gen(2:5,2)';
[n1OpfClass,n1OpfDetails,n1OpfDR,pfSuccess] = util.classifyCase(mpc,sp,genList,mpOption);
n1OpfST = pfSuccess && n1OpfClass(1) && n1OpfClass(2);
if mode
    fprintf('|N-1 SC ACOPF objective value:<strong>%3.2f $/MW</strong> |\n',results_N1.f);
    fprintf(strcat('|', repmat('-',1,chr-3),'|\n'))
    pretty_print(pfSuccess,n1OpfClass,chr)
    fprintf('Cost of security:\t\t%3.2f\n',((results_N1.f/results.f)-1)*100);%
end

% -----     OUTPUT     -----
if nargout >= 1
    milp.classification = milpClass;
    milp.details = milpDetails;
    milp.dr = milpDR;
    milp.stability = milpST;
    milp.price = value(obj);
    %milp.time available from caller (Yalmip.time)
    varargout{1} = milp;
end
if nargout >= 2
    ac_opf.classification = acopfClass;
    ac_opf.details = acopfDetails;
    ac_opf.dr = acopfDR;
    ac_opf.stability = acopfST;
    ac_opf.price = results.f;
    ac_opf.time = opfTime;
    varargout{2} = ac_opf;
end
if nargout >= 3
    n1opf.classification = n1OpfClass;
    n1opf.details = n1OpfDetails;
    n1opf.dr = n1OpfDR;
    n1opf.stability = n1OpfST;
    n1opf.price = results_N1.f;
    n1opf.time = n1opfTime;
    varargout{3} = n1opf;
end
end


function pretty_print(pfSuccess,classification,chr)
fprintf('|\tPowerFlow convergence:\t\t%d  |\n',pfSuccess)
fprintf('|\tACOPF constraints:\t\t%d  |\n',classification(1))
fprintf('|\tDamping Ratio constraints:\t%d  |\n',classification(2))
fprintf(strcat('|', repmat('=',1,chr-3),'|\n'))
end