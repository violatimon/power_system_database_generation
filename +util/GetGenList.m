function generatorList = GetGenList(mpc)
% return indeces of generators except slackbus
   slackIdx = mpc.bus(:,2) == 3; % bus type == ref
   slackBus = mpc.bus(slackIdx,1); % slack bus number
   generatorList = mpc.gen(:,1) ~= slackBus; % L 
end