% Concatenate two datasets of setpoints/stability, e.g. an equally spaced
% dataset throught the whole state space and a finer sampled equally spaced
% dataset around stable points.

% 1st db enrich 91.2 %
load(strcat(pwd,filesep,'database_files',filesep,'case14db_qLims2__11_03_1813.mat'))
classDetails_ = classDetails;
classification_ = classification;
setpoints_ = setpoints;
% Initial whole space sampling 0.167%
load(strcat(pwd,filesep,'database_files',filesep,'case14db_qLims2__10_20_1915.mat'))
classDetails_ = [classDetails; classDetails_];
classification_ = [classification; classification_];
setpoints_ = [setpoints; setpoints_];

% not good only unstable
% % case14db_qLims2__01_21_1225.mat
% load('/home/timon/Documents/powersys/power_system_database_generation/database_files/case14db_qLims2__01_21_1225.mat')
% classDetails_ = [classDetails; classDetails_];
% classification_ = [classification; classification_];
% setpoints_ = [setpoints; setpoints_];

% case14db_qLims2__01_21_1301.mat
% On the margin 14.844 %
load(strcat(pwd,filesep,'database_files',filesep,'case14db_qLims2__01_21_1301.mat'))
classDetails_ = [classDetails; classDetails_];
classification_ = [classification; classification_];
setpoints_ = [setpoints; setpoints_];

% case14db_qLims2__01_22_0907 small but good 30%
load(strcat(pwd,filesep,'database_files',filesep,'case14db_qLims2__01_22_0907.mat'))
classDetails_ = [classDetails; classDetails_];
classification_ = [classification; classification_];
setpoints_ = [setpoints; setpoints_];

% case14db_qLims2__01_22_1002.mat finally good 42 %
load(strcat(pwd,filesep,'database_files',filesep,'case14db_qLims2__01_22_1002.mat'))
classDetails_ = [classDetails; classDetails_];
classification_ = [classification; classification_];
setpoints_ = [setpoints; setpoints_];


%case14db_qLims2__01_21_1441.mat
% On the margin 10.118 %
load(strcat(pwd,filesep,'database_files',filesep,'case14db_qLims2__01_21_1441.mat'))
classDetails = [classDetails; classDetails_];
classification = [classification; classification_];
setpoints = [setpoints; setpoints_];

% Not good only unstable
% % case14db_qLims2__01_16_1931.mat
% load(pwd+'database_files/case14db_qLims2__01_16_1931.mat')
% classDetails = [classDetails; classDetails_];
% classification = [classification; classification_];
% setpoints = [setpoints; setpoints_];
% 
clear *_
clear dampingRatio

% %% Save to NN
% setpoints = setpoints./max(setpoints);
% writematrix(setpoints,'NN_input.csv')
% classification = [classification(:,1), ~classification(:,1)];
% writematrix(classification,'NN_output.csv')
