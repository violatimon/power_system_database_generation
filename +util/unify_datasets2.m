% Concatenate two datasets of setpoints/stability, e.g. an equally spaced
% dataset throught the whole state space and a finer sampled equally spaced
% dataset around stable points.

%space
cd('database_files\')
load('case14db_qLims0_qRelaxed_02_20_1200.mat')                      %0.079% case14db_qLims0_q_s_Relaxed_02_25_0933.mat
classDetails_ = classDetails;
classification_ = classification;
setpoints_ = setpoints;
dampingRatio_ = dampingRatio;

% load('case14db_qLims0_qRelaxed_02_20_0011.mat')  % whole event
% classDetails_ = [classDetails; classDetails_];
% classification_ = [classification; classification_];
% setpoints_ = [setpoints; setpoints_];
% dampingRatio_ = [dampingRatio; dampingRatio_];
load('case14db_qLims0_qRelaxed_02_20_1441.mat') % case14db_qLims0_q_s_Relaxed_02_25_0901
classDetails_ = [classDetails; classDetails_];
classification_ = [classification; classification_];
setpoints_ = [setpoints; setpoints_];
dampingRatio_ = [dampingRatio; dampingRatio_];

load('case14db_qLims0_qRelaxed_02_20_1507.mat') % whole event space  case14db_qLims0_q_s_Relaxed_02_25_0946.mat
classDetails_ = [classDetails; classDetails_];
classification_ = [classification; classification_];
setpoints_ = [setpoints; setpoints_];
dampingRatio_ = [dampingRatio; dampingRatio_];


load('case14db_qLims0_qRelaxed_02_20_1335.mat')                      %0.079%
classDetails = [classDetails; classDetails_];
classification = [classification; classification_];
setpoints = [setpoints; setpoints_];
dampingRatio = [dampingRatio; dampingRatio_];

% load('case14db_qLims0_qRelaxed_02_19_2212.mat')                         % 0%
% classDetails = [classDetails; classDetails_];
% classification = [classification; classification_];
% setpoints = [setpoints; setpoints_];
% dampingRatio = [dampingRatio; dampingRatio_];


% 
clear *_
cd ..

% %% Save to NN
% setpoints = setpoints./max(setpoints);
% writematrix(setpoints,'NN_input.csv')
% classification = [classification(:,1), ~classification(:,1)];
% writematrix(classification,'NN_output.csv')
