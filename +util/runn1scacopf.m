function results_N1 = runn1scacopf(mpc,mpOption)
% define named indices into bus, gen, branch matrices
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, RATE_C, ...
    TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST, ...
    ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, ...
    MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, ...
    QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;


mpc.bus(mpc.gen(:,GEN_BUS),VMAX) = mpc.gen(:,VG)+0.00001;
mpc.bus(mpc.gen(:,GEN_BUS),VMIN) = mpc.gen(:,VG)-0.00001;

con_list = mpc.contingencies; %[[5 7]];%%%
% indices of voltage set-points of generators
v_IDs = mpc.gen(:,GEN_BUS);

% generator indices 
ID_gen = find(mpc.gen(:,PMAX)-mpc.gen(:,PMIN)>=10^-4); %only look at generators
    
   
    
[maxP,id_maxP] = max(mpc.gen(:,PMAX)-mpc.gen(:,PMIN));
fprintf('setting slack bus to the largest generator: G%d\n',id_maxP);
mpc.bus(mpc.bus(:,2)==REF,2) = PV;
mpc.bus(mpc.gen(id_maxP,GEN_BUS),2) = REF;


[maxP2,id_maxP2] = max(mpc.gen(ID_gen,PMAX)-mpc.gen(ID_gen,PMIN));
% remove slack bus from ID_gen
% active power of generator compensates the 
ID_gen(id_maxP2) = [];
 
% build N-1 case, external function
mpc_N1 = test.build_N1(mpc,con_list,ID_gen,v_IDs);

opt_opf = mpOption;
opt_opf.opf.ac.solver='IPOPT';

results_N1=runopf(mpc_N1,opt_opf);

end