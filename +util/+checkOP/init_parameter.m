function parameter=  init_parameter()
%% Parameters: Source: Power System Small Signal Stability Analysis and Control- APPENDIX B; http://www.ieso.ca/Documents/caa/caa_SIAReport_2007-EX345.pdf
TdoBus1= 7.4;%sec D-Axis O.C. Transient Time Constant
TdoBus2= 6.1;
TdoBus3= 6.1;
TdoBus6= 4.75;
TdoBus8= 4.75; 

TdosBus1= 0.03;
TdosBus2= 0.04;
TdosBus3= 0.04;
TdosBus6= 0.06;
TdosBus8= 0.06;

TqoBus1= 0.31;%0.31;%0.3;%secQ-Axis O.C. Transient Time Constant
TqoBus2= 0.3;
TqoBus3= 0.3;
TqoBus6= 1.5;
TqoBus8= 1.5; 

TqosBus1= 0.033;
TqosBus2= 0.099;
TqosBus3= 0.099;
TqosBus6= 0.21;
TqosBus8= 0.21;

HBus1=5.148;%pu Interia
HBus2=6.54;
HBus3=6.54;
HBus6=5.06;
HBus8=5.06;

XdBus1=0.8979; %pu D-Axis Synchronous Reactance
XdBus2=1.05; %pu 
XdBus3=1.05; %pu 
XdBus6=1.25; %pu
XdBus8=1.25; %pu

XqBus1=0.646;%pu Q-Axis Synchronous Reactance
XqBus2=0.98;%pu
XqBus3=0.98;%pu
XqBus6=1.22;%pu
XqBus8=1.22;%pu

XdsBus1=0.2995;%pu D-Axis Transient Reactance
XdsBus2=0.185;%pu
XdsBus3=0.185;%pu
XdsBus6=0.232;%pu
XdsBus8=0.232;%pu

RsBus1=0.0031;%
RsBus2=0.0031;%
RsBus3=0.0031;%
RsBus6=0.0041;%
RsBus8=0.0041;%

XqsBus1=0.646;%pu Q-Axis Transient Reactance
XqsBus2=0.36;%pu
XqsBus3=0.36;%pu
XqsBus6=0.715;%pu
XqsBus8=0.715;%pu

KABus1=120;%20;% %Amplifier gain /AVR power stage gain
KABus2=20;%20;%
KABus3=20;%20;%
KABus6=20;%20;%
KABus8=20;%20;%

TABus1=0.02;%0.01;%Amplifier time constant /AVR power stage time constant - sec
TABus2=0.02;%0.01;%Amplifier time constant /AVR power stage time constant - sec
TABus3=0.02;%0.01;%Amplifier time constant /AVR power stage time constant - sec
TABus6=0.02;%0.01;%Amplifier time constant /AVR power stage time constant - sec
TABus8=0.02;%0.01;%Amplifier time constant /AVR power stage time constant - sec

KeBus1=1; %Exciter field proportional constant
KeBus2=1; %Exciter field proportional constant
KeBus3=1; %Exciter field proportional constant
KeBus6=1; %Exciter field proportional constant
KeBus8=1; %Exciter field proportional constant

TeBus1=0.19;%%Exciter time constant - sec
TeBus2=1.98;%
TeBus3=1.98;%
TeBus6=0.7;%
TeBus8=0.7;%

KFBus1=0.0012;
KFBus2=0.001;
KFBus3=0.001;
KFBus6=0.001;
KFBus8=0.001;

TFBus1=1;%
TFBus2=1;%
TFBus3=1;%
TFBus6=1;%
TFBus8=1;%

DBus1=2;%0.046;
DBus2=2;%0.046;
DBus3=2;%0.046;
DBus6=2;%0.046;
DBus8=2;%0.046;

Vrmax1=9.99;
Vrmax2=2.05;
Vrmax3=1.7;
Vrmax6=2.2;
Vrmax8=2.2;


Aex=0.0006;
Bex=0.9;

Omegas=1;
OmegaBase=2*pi*60;

XdssBus1=0.23;
XdssBus2=0.13;
XdssBus3=0.13;
XdssBus6=0.12;
XdssBus8=0.12;

XqssBus1=0.4;
XqssBus2=0.13;
XqssBus3=0.13;
XqssBus6=0.12;
XqssBus8=0.12;

XlBus1=0.2396;
XlBus2=0;
XlBus3=0;
XlBus6=0.134;
XlBus8=0.134;

TrBus1=0.001;
TrBus2=0.001;
TrBus3=0.001;
TrBus6=0.001;
TrBus8=0.001;

gammaq1Bus1=(XqssBus1-XlBus1)/(XqsBus1-XlBus1);
gammaq2Bus1=(1-gammaq1Bus1)/(XqsBus1-XlBus1);
gammad1Bus1=(XdssBus1-XlBus1)/(XdsBus1-XlBus1);
gammad2Bus1=(1-gammad1Bus1)/(XdsBus1-XlBus1);

gammaq1Bus2=(XqssBus2-XlBus2)/(XqsBus2-XlBus2);
gammaq2Bus2=(1-gammaq1Bus2)/(XqsBus2-XlBus2);
gammad1Bus2=(XdssBus2-XlBus2)/(XdsBus2-XlBus2);
gammad2Bus2=(1-gammad1Bus2)/(XdsBus2-XlBus2);

gammaq1Bus3=(XqssBus3-XlBus3)/(XqsBus3-XlBus3);
gammaq2Bus3=(1-gammaq1Bus3)/(XqsBus3-XlBus3);
gammad1Bus3=(XdssBus3-XlBus3)/(XdsBus3-XlBus3);
gammad2Bus3=(1-gammad1Bus3)/(XdsBus3-XlBus3);

gammaq1Bus6=(XqssBus6-XlBus6)/(XqsBus6-XlBus6);
gammaq2Bus6=(1-gammaq1Bus6)/(XqsBus6-XlBus6);
gammad1Bus6=(XdssBus6-XlBus6)/(XdsBus6-XlBus6);
gammad2Bus6=(1-gammad1Bus6)/(XdsBus6-XlBus6);

gammaq1Bus8=(XqssBus8-XlBus8)/(XqsBus8-XlBus8);
gammaq2Bus8=(1-gammaq1Bus8)/(XqsBus8-XlBus8);
gammad1Bus8=(XdssBus8-XlBus8)/(XdsBus8-XlBus8);
gammad2Bus8=(1-gammad1Bus8)/(XdsBus8-XlBus8);


parameter=[TdoBus1,TdoBus2,TdoBus3,TdoBus6,TdoBus8,TqoBus1,TqoBus2,TqoBus3,TqoBus6,TqoBus8,HBus1,HBus2,HBus3,HBus6,HBus8,XdBus1,XdBus2,XdBus3,XdBus6,XdBus8,XqBus1,XqBus2,XqBus3,XqBus6,XqBus8,XdsBus1,XdsBus2,XdsBus3,XdsBus6,XdsBus8,RsBus1,RsBus2,RsBus3,RsBus6,RsBus8,XqsBus1,XqsBus2,XqsBus3,XqsBus6,XqsBus8,KABus1,KABus2,KABus3,KABus6,KABus8,TABus1,TABus2,TABus3,TABus6,TABus8,KeBus1,KeBus2,KeBus3,KeBus6,KeBus8,TeBus1,TeBus2,TeBus3,TeBus6,TeBus8,KFBus1,KFBus2,KFBus3,KFBus6,KFBus8,TFBus1,TFBus2,TFBus3,TFBus6,TFBus8,DBus1,DBus2,DBus3,DBus6,DBus8,Aex,Bex,Omegas,gammaq1Bus1,gammaq2Bus1,gammad1Bus1,gammad2Bus1,gammaq1Bus2,gammaq2Bus2,gammad1Bus2,gammad2Bus2,gammaq1Bus3,gammaq2Bus3,gammad1Bus3,gammad2Bus3,gammaq1Bus6,gammaq2Bus6,gammad1Bus6,gammad2Bus6,gammaq1Bus8,gammaq2Bus8,gammad1Bus8,gammad2Bus8,TrBus1,TrBus2,TrBus3,TrBus6,TrBus8,Vrmax1,Vrmax2,Vrmax3,Vrmax6,Vrmax8,XdssBus1,XdssBus2,XdssBus3,XdssBus6,XdssBus8,XlBus1,XlBus2,XlBus3,XlBus6,XlBus8,TdosBus1,TdosBus2,TdosBus3,TdosBus6,TdosBus8,TqosBus1,TqosBus2,TqosBus3,TqosBus6,TqosBus8,XqssBus1,XqssBus2,XqssBus3,XqssBus6,XqssBus8,OmegaBase];


