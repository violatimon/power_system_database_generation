function mpc = case14_mod(Pgen,Load,status,bus_type)
%CASE14    Power flow data for IEEE 14 bus test case.
%   Please see CASEFORMAT for details on the case file format.
%   This data was converted from IEEE Common Data Format
%   (ieee14cdf.txt) on 15-Oct-2014 by cdf2matp, rev. 2393
%   See end of file for warnings generated during conversion.
%
%   Converted from IEEE CDF file from:
%       http://www.ee.washington.edu/research/pstca/
% 
%  08/19/93 UW ARCHIVE           100.0  1962 W IEEE 14 Bus Test Case

%% check if any bus fault at a generator bus exists
gen_status=ones(1,5);
if bus_type(1)==4
    gen_status(1)=-1;
else
end
if bus_type(2)==4
    gen_status(2)=-1;
else
end
if bus_type(3)==4
    gen_status(3)=-1;
else
end
if bus_type(6)==4
    gen_status(4)=-1;
else
end
if bus_type(8)==4
    gen_status(5)=-1;
else
end

%% MATPOWER Case Format : Version 2
mpc.version = '2';

%%-----  Power Flow Data  -----%%
%% system MVA base
mpc.baseMVA = 100;

%% bus data
%	bus_i	type	Pd      Qd      Gs	Bs	area	Vm	Va	baseKV	zone	Vmax	Vmin
mpc.bus = [
	1	bus_type(1)	Load(1,1)     Load(1,2)       0	0	1	1.06	0       69   1	1.06	0.94;
	2	bus_type(2)	Load(2,1)	Load(2,2)	0	0	1   1.045	-4.98	69	1	1.06	0.94;
	3	bus_type(3)	Load(3,1)	Load(3,2)      0	0	1	1.01	-12.72	69   1	1.06	0.94;
	4	bus_type(4)	Load(4,1)	Load(4,2)	0	0	1   1.019	-10.33	69	1	1.06	0.94;
	5	bus_type(5)	Load(5,1)	Load(5,2)     0	0	1	1.02	-8.78	69   1	1.06	0.94;
	6	bus_type(6)	Load(6,1)	Load(6,2)     0	0	1	1.07	-14.22	13.8   1	1.06	0.94;
	7	bus_type(7)	Load(7,1)	Load(7,2)      0	0	1	1.062	-13.37	13.8   1	1.06	0.94;
	8	bus_type(8)	Load(8,1)	Load(8,2)       0	0	1	1.09	-13.36	18   1	1.06	0.94;
	9	bus_type(9)	Load(9,1)	Load(9,2)	0	0	1   1.056	-14.94	13.8	1	1.06	0.94;
	10	bus_type(10)	Load(10,1)	Load(10,2)     0	0	1	1.051	-15.1	13.8   1	1.06	0.94;
	11	bus_type(11)	Load(11,1)	Load(11,2)     0	0	1	1.057	-14.79	13.8   1	1.06	0.94;
	12	bus_type(12)	Load(12,1)	Load(12,2)     0	0	1	1.055	-15.07	13.8   1	1.06	0.94;
	13	bus_type(13)	Load(13,1)	Load(13,2)     0	0	1	1.05	-15.16	13.8   1	1.06	0.94;
	14	bus_type(14)	Load(14,1)	Load(14,2)       0	0	1	1.036	-16.04	13.8   1	1.06	0.94;
];

%% generator data
%	bus	Pg	Qg	Qmax	Qmin	Vg	mBase	status	Pmax	Pmin	Pc1	Pc2	Qc1min	Qc1max	Qc2min	Qc2max	ramp_agc	ramp_10	ramp_30	ramp_q	apf
mpc.gen = [
	1	Pgen(1,1)	-16.9	990	-990	1.06	100    gen_status(1)	615	0	0	0	0	0	0	0	0	0	0	0	0;
	2	Pgen(2,1)	42.4	50	-40	1.045	100     gen_status(2)	60	0	0	0	0	0	0	0	0	0	0	0	0;
	3	Pgen(3,1)	23.4	40	0	1.01	100     gen_status(3)	60	0	0	0	0	0	0	0	0	0	0	0	0;
	6	Pgen(4,1)	12.2	24	-6	1.02	100     gen_status(4)	25	0	0	0	0	0	0	0	0	0	0	0	0;
	8	Pgen(5,1)	17.4	24	-6	1.01	100     gen_status(5)	25	0	0	0	0	0	0	0	0	0	0	0	0;
];

mpc.branch = [
	1	2	0.01938	0.05917	0.0528	0	0	0	0	0	status(1)	-360	360;
	1	5	0.05403	0.22304	0.0492	0	0	0	0	0	status(2)	-360	360;
	2	3	0.04699	0.19797	0.0438	0	0	0	0	0	status(3)	-360	360;
	2	4	0.05811	0.17632	0.034	0	0	0	0	0	status(4)	-360	360;
	2	5	0.05695	0.17388	0.0346	0	0	0	0	0	status(5)	-360	360;
	3	4	0.06701	0.17103	0.0128	0	0	0	0	0	status(6)	-360	360;
	4	5	0.01335	0.04211	0	0	0	0	0	0	status(7)	-360	360;
	4	7	0	0.20912	0	0	0	0	0.978	0	status(8)	-360	360;
	4	9	0	0.55618	0	0	0	0	0.969	0	status(9)	-360	360;
	5	6	0	0.25202	0	0	0	0	0.932	0	status(10)	-360	360;
	6	11	0.09498	0.1989	0	0	0	0	0	0	status(11)	-360	360;
	6	12	0.12291	0.25581	0	0	0	0	0	0	status(12)	-360	360;
	6	13	0.06615	0.13027	0	0	0	0	0	0	status(13)	-360	360;
	7	8	0	0.17615	0	0	0	0	0	0	status(14)	-360	360;
	7	9	0	0.11001	0	0	0	0	0	0	status(15)	-360	360;
	9	10	0.03181	0.0845	0	0	0	0	0	0	status(16)	-360	360;
	9	14	0.12711	0.27038	0	0	0	0	0	0	status(17)	-360	360;
	10	11	0.08205	0.19207	0	0	0	0	0	0	status(18)	-360	360;
	12	13	0.22092	0.19988	0	0	0	0	0	0	status(19)	-360	360;
	13	14	0.17093	0.34802	0	0	0	0	0	0	status(20)	-360	360;
];
mpc.branch(:,6)=100.*ones(size(mpc.branch(:,6),1),1);
%%-----  OPF Data  -----%%
%% generator cost data
%	1	startup	shutdown	n	x1	y1	...	xn	yn
%	2	startup	shutdown	n	c(n-1)	...	c0
mpc.gencost = [
	2	0	0	3	0.0430292599	20	0;
	2	0	0	3	0.25	20	0;
	2	0	0	3	0.01	40	0;
	2	0	0	3	0.01	40	0;
	2	0	0	3	0.01	40	0;
];

%% bus names
mpc.bus_name = {
	'Bus 1     HV';
	'Bus 2     HV';
	'Bus 3     HV';
	'Bus 4     HV';
	'Bus 5     HV';
	'Bus 6     LV';
	'Bus 7     ZV';
	'Bus 8     TV';
	'Bus 9     LV';
	'Bus 10    LV';
	'Bus 11    LV';
	'Bus 12    LV';
	'Bus 13    LV';
	'Bus 14    LV';
};

% Warnings from cdf2matp conversion:
%
% ***** check the title format in the first line of the cdf file.
% ***** Qmax = Qmin at generator at bus    1 (Qmax set to Qmin + 10)
% ***** MVA limit of branch 1 - 2 not given, set to 0
% ***** MVA limit of branch 1 - 5 not given, set to 0
% ***** MVA limit of branch 2 - 3 not given, set to 0
% ***** MVA limit of branch 2 - 4 not given, set to 0
% ***** MVA limit of branch 2 - 5 not given, set to 0
% ***** MVA limit of branch 3 - 4 not given, set to 0
% ***** MVA limit of branch 4 - 5 not given, set to 0
% ***** MVA limit of branch 4 - 7 not given, set to 0
% ***** MVA limit of branch 4 - 9 not given, set to 0
% ***** MVA limit of branch 5 - 6 not given, set to 0
% ***** MVA limit of branch 6 - 11 not given, set to 0
% ***** MVA limit of branch 6 - 12 not given, set to 0
% ***** MVA limit of branch 6 - 13 not given, set to 0
% ***** MVA limit of branch 7 - 8 not given, set to 0
% ***** MVA limit of branch 7 - 9 not given, set to 0
% ***** MVA limit of branch 9 - 10 not given, set to 0
% ***** MVA limit of branch 9 - 14 not given, set to 0
% ***** MVA limit of branch 10 - 11 not given, set to 0
% ***** MVA limit of branch 12 - 13 not given, set to 0
% ***** MVA limit of branch 13 - 14 not given, set to 0
