mycl = parcluster('local');
mycl.JobStorageLocation='/work3/s182215/Jobs/';
mypool = parpool(mycl,20);

path=pwd;
% Result path
ResPath = strcat(path, '/Results/DBc6/');

addpath(genpath('/work3/s182215/matpower6.0/'));
addpath(genpath('/work3/s182215/Matlab_scripts/'));

data = loadcase(util.case14_wind);
mpc = parallel.pool.Constant(data);

Res = [];

parfor cont = 1:11
disp(cont)

mpc_torun = mpc.Value;

[DB, PG, assess, secure_ratio] = generate_DB_c6(mpc_torun, 1, cont);

results = [DB, PG, assess];

lenDB = size(results, 1);

Res = results;

cont_str = num2str(cont);
res_name = strcat('Res', cont_str);
parsave(fullfile(ResPath, res_name), Res);


end
