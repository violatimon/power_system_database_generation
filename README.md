# Power system database generation for IEEE14 bus system
Clone the repository using 'git clone --recurse-submodules' to include all dependencies:
*  neural network (NN training files)
*  MATPOWER
*  YALMIP

*GUROBI* is not included, it has to be set-up manually
  

## Main script, utility functions and .mat database 

1. databse generation main script
   - generate database
   - [gen_database_bf2](gen_database_bf2.m)

2. util package
   - all utility functions
   - [checkOP](+util/checkOP.m) to check small signal stability
   
3. database
   - [case14db](case14db.mat) file
 
---

This repo strictly follows the guidelines set by [conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/).
