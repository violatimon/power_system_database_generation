% Generate database w/ generator setpoints and classification of o.p.
% Output: classification - the columns are: stable, ac_opf stable, SSS
%         classDetails  - detailed struct of N-1 operating point stability
%         setPoints     - Generator 2-5 setpoints
%
% Note: all caps variable names are cleared at the end of the script
%% ----- Set up -----
tic
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, RATE_C, TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST,  ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;

% load case
mpcInit = util.case14_wind;

% matpower options - surpress output of runpf
mpOption = mpoption;
mpOption.verbose = 0;
mpOption.out.all = 0;

qOpt = 2;
mpOption.pf.enforce_q_lims = qOpt;
% 0 - do not enforce -> no feasable point, Q never satisfied in all N-1
% scenarios
% 1 - enforce limits, simultaneous -> ?
% 2 - enforce, one at a time
%

genList = util.GetGenList(mpcInit);% generator indeces without slack
genPRanges = mpcInit.gen(genList, PMAX) - mpcInit.gen(genList, PMIN); % P range for each 'free' generator

cont = mpcInit.contingencies; % list of contingencies to be checked
nb = length(mpcInit.branch(:,1));

mpc = mpcInit;
warning('off', 'all') % surpress checkOP warnings (singular matrix)
%% ----- setpoints -----
% steps of generator operating points
step = 0.5;

PMX = mpc.gen(2:5,PMAX);
PMN = mpc.gen(2:5,PMIN);

[p2,p3,p4,p5] = ndgrid(PMN(1):step:PMX(1),PMN(2):step:PMX(2),PMN(3):step:PMX(3),PMN(4):step:PMX(4));
setpoints = [p2(:) p3(:) p4(:) p5(:)];


%% ----- initialize output -----
n = size(setpoints, 1);
fprintf('Checking %d setpoints.\n',n)
classification = nan(n,2); % [AC-OPF constraints, small sig stab]
classDetails = cell(n,1);
dampingRatio = nan(n,1);

%% ----- classification -----
fprintf(1,'\nClassifying setpoints.\n')

parfor iter = 1:n
%for iter = 1:n
    [classification(iter,:),classDetails{iter},dampingRatio(iter)] = util.classifyCase(mpc,setpoints(iter,:),genList,mpOption);
end
%% ----- Output -----
fprintf('\nChecked %d cases.\n', n)
classification = [((classification(:,1) == 1) & (classification(:,2) == 1)), classification(:,1), classification(:,2)];
fileName = strcat('case14db','_qLims',num2str(qOpt),'_', datestr(now(),'_mm_dd_HHMM'),'.mat');
save(fileName,'classification', 'setpoints','classDetails','dampingRatio');
fprintf('Output file: %s\n',fileName)
%writematrix(setpoints,'NN_input.csv')
%writematrix(classification(:,2:3),'NN_output.csv')
%% ----- Tear down -----
warning('on', 'all') % surpress checkOP warnings
% get rid of matpower vars for easier debug
clear -regexp ^[^a-z]*$
toc
