#!/bin/sh
#BSUB -q hpc
#BSUB -J gen_dataset_bf
#BSUB -n 24
#BSUB -R "span[hosts=1]"
#BSUB -R "rusage[mem=10GB]"
#BSUB -M 12GB
#BSUB -W 10:00
#BSUB -u s182215@student.dtu.dk
#BSUB -B
#BSUB -N
#BSUB -o /db_gen/log/Output_%J.out
#BSUB -e /db_gen/log/Error_%J.err

matlab -nodisplay -nosplash -nodesktop -r "run('~/db_gen/power_system_database_generation/run_gen_dataset_bf');exit;"
#matlab -nodisplay -r run_gen_dataset_bf -logfile /db_gen/log/gen_dataset_bf
