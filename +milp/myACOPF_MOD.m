function results_opf = ACOPF_MOD(mpc,varargin)

%% Load indices
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, ...
    RATE_C, TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST, ...
    ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;
[PW_LINEAR, POLYNOMIAL, MODEL, STARTUP, SHUTDOWN, NCOST, COST] = idx_cost;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, ...
    MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, ...
    QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;


%% READ DATA
% mpc = swiss_acopf;

bus     = mpc.bus;
gen     = mpc.gen;
branch  = mpc.branch;
gencost = mpc.gencost;
baseMVA = mpc.baseMVA;

vmin = bus(:,VMIN);
vmax = bus(:,VMAX);

%vmin(1:3) = [1.05; 1.05; 1.07];
%vmax(1:3) = [1.05; 1.05; 1.07];

nb = size(bus,1);
ng = size(gen,1);
nl = size(branch,1);

ref = find(mpc.bus(:,BUS_TYPE) == REF);
pv  = find(mpc.bus(:,BUS_TYPE) == PV);
pq  = find(mpc.bus(:,BUS_TYPE) == PQ);

%% COMPUTE YBUS MATRIX AND AUXILIARY MATRICES
[Ybus, Yf, Yt] = makeYbus(baseMVA, bus, branch);
Gbus = real(Ybus);
Bbus = imag(Ybus);

map_n2g = zeros(nb,ng); % Node to generator incidence matrix of size (nb x ng)
for g = 1:ng
    map_n2g(gen(g,GEN_BUS),g) = 1;
end

%% ADD VARIABLES
P  = sdpvar(ng,1);   % Active power generation
Q  = sdpvar(ng,1);   % Reactive power generation
VM = sdpvar(nb,1);   % Nodal voltage magnitudes
VA = sdpvar(nb,1);   % Nodal voltage angles
V  = VM.*exp(1j*VA); % Complex voltage

%% ADD CONSTRAINTS

% Active power balance
P_BALANCE = [ map_n2g*P - bus(:,PD)/baseMVA == real(V.*conj(Ybus*V)) ];

% Reactive power balance
Q_BALANCE = [ map_n2g*Q - bus(:,QD)/baseMVA == imag(V.*conj(Ybus*V)) ];

% Line flows "FROM-TO" AND  "TO-FROM"
if sum(branch(:,RATE_A))>0.1
    SF_LIMITS = [ real(V(branch(:,F_BUS)).*conj(Yf*V)).^2 + imag(V(branch(:,F_BUS)).*conj(Yf*V)).^2 <= (branch(:,RATE_A)/baseMVA).^2 ];
    ST_LIMITS = [ real(V(branch(:,T_BUS)).*conj(Yt*V)).^2 + imag(V(branch(:,T_BUS)).*conj(Yt*V)).^2 <= (branch(:,RATE_A)/baseMVA).^2 ];
else
    SF_LIMITS = [];
    ST_LIMITS = [];
end

% Active and reactive generation limits
PG_LIMITS = [ P >= gen(:,PMIN)/baseMVA;
    P <= gen(:,PMAX)/baseMVA ];
QG_LIMITS = [ Q >= gen(:,QMIN)/baseMVA;
    Q <= gen(:,QMAX)/baseMVA ];

% Voltage magnitude limits
VM_LIMITS = [ VM >= vmin;
    VM <= vmax ];

% Angle at reference bus
VA_REF = [ VA(ref) == 0 ];

% Constraint vector
constraints = [
    P_BALANCE;
    Q_BALANCE;
    SF_LIMITS;
    ST_LIMITS;
    PG_LIMITS;
    QG_LIMITS;
    VM_LIMITS;
    VA_REF];

% Objective function
if gencost(1,NCOST) == 2
    objective = gencost(:,NCOST+1)'*P*baseMVA+sum(gencost(:,NCOST+2));
end
if mpc.gencost(1,NCOST) == 3
    objective = gencost(:,NCOST+1)'*(P.*P)*baseMVA^2 + gencost(:,NCOST+2)'*P*baseMVA+sum(gencost(:,NCOST+3));
end

% Optimize
options = sdpsettings('solver','fmincon',varargin{:});%,'verbose',0); % TODO: input parser option for debug/log mode
results = optimize(constraints, objective, options);

results_opf = mpc;
% set active power
results_opf.gen(:,PG) = value(P)*baseMVA;
% set reactive power
results_opf.gen(:,QG) = value(Q)*baseMVA;
%set terminal voltage
results_opf.gen(:, VG) = value(VM(mpc.gen(:,GEN_BUS)));%value(VM(1:3));
% set voltage magnitude
results_opf.bus(:,8) = value(VM);
% set voltage angle
results_opf.bus(:,9) = value(VA)*180/pi;

if results.problem == 0
    results_opf.success = 1;
else
    results_opf.success = 0;
    
end

results_opf.f = value(objective);
results_opf.P = value(P)*baseMVA;
results_opf.Q = value(Q)*baseMVA;
results_opf.VM = value(VM);
results_opf.VA = value(VA);
results_opf.solvertime = results.solvertime;

end
