%% -----  NN data  -----
path = '~/Documents/powersys/neural_network/balanced_set/';
path2wb = strcat(path,'output/');

% load data from Python
NN_input = csvread(strcat(path,'NN_input.csv'));
safe_log_ = csvread(strcat(path,'NN_output.csv'));      % manipulate out size
safe_log = safe_log_(:,1);
NN_output=safe_log_(:,1);

W_input = csvread(strcat(path2wb ,'W0_p.csv')).';
W_output = csvread(strcat(path2wb,'W3_p.csv')).';
W{1} = csvread(strcat(path2wb ,'W1_p.csv')).';
W{2} = csvread(strcat(path2wb ,'W2_p.csv')).';

b{1} = csvread(strcat(path2wb ,'b0_p.csv'));
b{2} = csvread(strcat(path2wb ,'b1_p.csv'));
b{3} = csvread(strcat(path2wb ,'b2_p.csv'));
b{4} = csvread(strcat(path2wb ,'b3_p.csv'));

z0_hat  = W_input*NN_input(1,:)' + b{1};
z0 = max(0,z0_hat);
z1_hat = W{1}*z0 + b{2};
z1 = max(0,z1_hat);
z2_hat = W{2}*z1 + b{3};
z2 = max(0,z2_hat);
z3 = W_output*z2+ b{4};
%z3_hat = max(0,z3);



% z1 = W1 * x + b1
% z1_hat  = max(z1,0)
% z2 = W2 * z2 + b2
% ..
% Output: y = W_last*z_last + b_last
% (Note that the ReLU is not applied to the o

disp(z3)