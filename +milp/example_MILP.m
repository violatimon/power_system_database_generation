close all;
clear all;

% install GUROBI and YALMIP

addpath(genpath('C:\gurobi811\win64'));
addpath(genpath('C:\Users\Andreas\Gurobi'));

% define the neural network size
hidden_layer = 100;
ReLU_layers = 4;

display('here enter the path where you saved the NN output. Make sure size of NN fits');

path = 'F:\OneDrive\PhD\Projects\Neural Networks for AC Power Flow\Python_TensorFlow\162bus\';

% load data from Python
Input_NN = csvread(strcat(path,'NN_input.csv'));
safe_log_ = csvread(strcat(path,'NN_output.csv'));
safe_log = safe_log_(:,1);
NN_output=safe_log_(:,1);
% we have to invert the weight matrices
W_input = csvread(strcat(path,'W0_p.csv')).';
W_output = csvread(strcat(path,'W4_p.csv')).'; % not clear how the indexing works here (going from layer 1 to layer 2)
% for i = 1:ReLU_layers-1
%     W{i} = net.LW{1+i,i};
% end

W{1} = csvread(strcat(path,'W1_p.csv')).';
W{2} = csvread(strcat(path,'W2_p.csv')).';
W{3} = csvread(strcat(path,'W3_p.csv')).';
bias{1} = csvread(strcat(path,'b0_p.csv')); %net.b; % bias
bias{2} = csvread(strcat(path,'b1_p.csv'));
bias{3} = csvread(strcat(path,'b2_p.csv'));
bias{4} = csvread(strcat(path,'b3_p.csv'));
bias{5} = csvread(strcat(path,'b4_p.csv'));

% options for post-processing of neural network
interval_arithmetic = true;

% interval arithmetic is a technique to tighten the bounds on the integer
% formulation of neural networks; this speeds up the solving of the MILP

x_0_up = ones(hidden_layer,1,ReLU_layers)*(1000);% upper bound on x_0 (Here we will need to use some bound tightening)
x_0_lp = ones(hidden_layer,1,ReLU_layers)*(-1000);% lower bound on x_0 (Here we will need to use some bound tightening)

if interval_arithmetic == true
    
    if interval_arithmetic == true
        % use interval arithmetic to compute tighter bounds
        % initial input bounds
        u_init = ones(17,1);
        l_ini = zeros(17,1);
        x_0_up(:,1,1) = max(W_input,0)*u_init+min(W_input,0)*l_ini+bias{1};
        x_0_lp(:,1,1) = min(W_input,0)*u_init+max(W_input,0)*l_ini+bias{1};
        for j = 1:ReLU_layers-1
            x_0_up(:,1,j+1) = max(W{j},0)*max(x_0_up(:,1,j),0)+min(W{j},0)*max(x_0_lp(:,1,j),0)+bias{j+1};
            x_0_lp(:,1,j+1) = min(W{j},0)*max(x_0_up(:,1,j),0)+max(W{j},0)*max(x_0_lp(:,1,j),0)+bias{j+1};
        end
        
    end
end 

size_input = size(Input_NN,2);

LP_relax = false;
% construct otpimization problem of neural network
u_NN = sdpvar(size_input,1);
if LP_relax == true %integer relaxation
    ReLU_0 = sdpvar(hidden_layer,1,ReLU_layers);
else
    ReLU_0 = binvar(hidden_layer,1,ReLU_layers);
end

x_0 = sdpvar(hidden_layer,1,ReLU_layers);
x_0_ReLU = sdpvar(hidden_layer,1,ReLU_layers);
y = sdpvar(2,1);
eps = sdpvar(1,1);

% HERE for MILP OPF we will have the cost function c2*x.^2 + c1*x + c0
% with costs extracted from the matpower mpc file 
% you will not need eps as a variable

obj = eps;

constr = [];
%input restrictions
constr = [constr;...
    0<=u_NN<=1];

%input layer
constr = [constr; ...
    x_0(:,:,1) == W_input*u_NN + bias{1}];
for i = 1:ReLU_layers
% ReLU (rewriting the max function)
constr = [constr; ...
    x_0_ReLU(:,:,i) <= x_0(:,:,i) - x_0_lp(:,:,i).*(1-ReLU_0(:,:,i));...1
    x_0_ReLU(:,:,i) >= x_0(:,:,i);...
    x_0_ReLU(:,:,i) <= x_0_up(:,:,i).*ReLU_0(:,:,i);...
    x_0_ReLU(:,:,i) >= 0];
end
for i = 1:ReLU_layers-1
    constr = [constr; ...
        x_0(:,:,i+1) == W{i}*x_0_ReLU(:,:,i) + bias{i+1}];
end 
if LP_relax == true %integer relaxation
% % integer relaxation
 constr = [constr; ...
     0<= ReLU_0 <=1 ];
end
% output layer
constr = [constr; ...
    y == W_output * x_0_ReLU(:,:,end) + bias{end}];


u_NN_0 = zeros(size_input,1);

constr = [constr; ...
    -1 <= eps <= 1];
% l - inf norm
constr = [constr; ...
     - eps <= u_NN - u_NN_0 <= eps];

constr = [constr; ...
    y(1) >= y(2)]; % classification as safe
%    y(2) >= y(1)]; % classification as unsafe

% set Gurobi as solver
options = sdpsettings('solver','gurobi');
% solve
diagnostics = optimize(constr,obj,options);

value(obj)




    
 


