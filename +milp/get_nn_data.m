function [W_input,W,W_output,bias, NN_input, NN_output] = get_nn_data(path)
% if ispc
%     path2wb = path;
% else
    path2wb = strcat(path,'output/');
% end
% define the neural network size

% load data from Python
NN_input = csvread(strcat(path,'NN_input.csv'));
safe_log_ = csvread(strcat(path,'NN_output.csv'));      % manipulate out size
%safe_log = safe_log_(:,1);
NN_output=safe_log_(:,1);

% we have to invert the weight matrices
W_input = csvread(strcat(path2wb ,'W0_p.csv')).';
W_output = csvread(strcat(path2wb,'W3_p.csv')).';
W{1} = csvread(strcat(path2wb ,'W1_p.csv')).';
W{2} = csvread(strcat(path2wb ,'W2_p.csv')).';

bias{1} = csvread(strcat(path2wb ,'b0_p.csv'));
bias{2} = csvread(strcat(path2wb ,'b1_p.csv'));
bias{3} = csvread(strcat(path2wb ,'b2_p.csv'));
bias{4} = csvread(strcat(path2wb ,'b3_p.csv'));

end
