function [l,pGeneration] = getLossSensitivity(mpc,mpopt,DELTA_P,varargin)
% GETLOSSSENSITIVITY - returns the linear loss coefficients for N
% generators as an Nx1 vector; returns the generator dispatches as an Nx1
% vector.

PG = 2;
PD = 3;
%% 0 step -> calculate ACOPF without losses
pfResults = milp.myACOPF_MOD(mpc,varargin{:});

pGeneration = pfResults.gen(:, PG);
genlist = util.GetGenList(mpc); % 0 for slack 1 for rest
NG = length(genlist);
l = nan(NG,1);

%% 1 st step -> calculate Pi_DC
l(1) = sum(pGeneration) - sum(mpc.bus(:, PD)); % PD = 3 real power demand

%% 2 nd step -> ACOPF to calculate P_L^0, \gamma
for i = 2:NG
    mpcTemp = mpc;
    mpcTemp.gen(:, PG) = pGeneration;
    mpcTemp.gen(i, PG) = pGeneration(i) + DELTA_P;

    pf = runpf(mpcTemp, mpopt);
    loss = sum(pf.gen(:, PG)) - sum(mpc.bus(:, PD));
    l(i) = (loss - l(1))/DELTA_P; % don't divide this one with baseMVA, is delta P is given in MW
end
pGeneration = pGeneration./mpc.baseMVA;
l(1) = l(1)/mpc.baseMVA;


end