load('MILP_PRICE_CHANGES_QS3_04_03_20T21_05_23')

% only G1-3 changes ,G4-5 fixed at zero

% x axis show the sum of cost function
% y axis show the sum of generator dispatch
nEps = 3;
baseMVA = 100;
genRange = [60 60 25 25];
lenEps = length(epsArray);
eps = epsArray(nEps);
genSetPoints = [pslack.*baseMVA,gsetpoints.*genRange];

% use only every n th value AND filter out non stable points
stabIdx = stability(nEps:lenEps:end,:);
stabIdx = boolean(stabIdx);
setP = genSetPoints(nEps:lenEps:end,:);
colSumP= sum(setP(stabIdx),2);
colSumCost = sum(spaceGrid(stabIdx),2);

% plot gDispatch vs price sum
f = figure('Name','Price sum vs G dispatch');
ax = axes;
hold on
x = sum(spaceGrid,2);
ph = cell(1,size(genSetPoints,2));
for i =1:size(genSetPoints,2)
    ph{i} = stairs(genSetPoints(nEps:lenEps:end,i),'DisplayName',"G"+i,'LineWidth',1.5);
end
legend

% scatter(colSumCost,colSumP)
% xlabel('Cost function increase')
% ylabel('Generator dispatch sum')

%% show one generator only
figure
c = zeros(length(stabIdx),3);
c(stabIdx,:) = repmat([0 1 0],sum(stabIdx),1);
c(~stabIdx,:) = repmat([1 0 0],sum(~stabIdx),1);
scatter3(setP(:,1),setP(:,2),setP(:,3),20,c);
xlabel('g1')
ylabel('g2')
zlabel('g3')
%% PLOT ALL 
clear 
load('MILP_PRICE_CHANGES_QS3_04_03_20T21_05_23')
if (length(epsArray) <= 4)
    % only G1-3 changes ,G4-5 fixed at zero

    % x axis show the sum of cost function
    % y axis show the sum of generator dispatch

    baseMVA = 100;
    genRange = [60 60 25 25];
    lenEps = length(epsArray);

    f = figure('Name','changing epsilon');
    ax = axes;
    hold on
    mkr = {'o','s','^','d'};
    h = cell(4,1);
    for i=1:lenEps
        eps = epsArray(i);
        genSetPoints = [pslack.*baseMVA,gsetpoints.*genRange];

        % use only every n th value AND filter out non stable points
        stabIdx = stability(i:lenEps:end,:);
        stabIdx = boolean(stabIdx);
        setP = genSetPoints(i:lenEps:end,:);
        colSumP= sum(setP(stabIdx),2);
        colSumCost = sum(spaceGrid(stabIdx),2);

        c = zeros(length(stabIdx),3);
        c(stabIdx,:) = repmat([0 1 0],sum(stabIdx),1);
        c(~stabIdx,:) = repmat([1 0 0],sum(~stabIdx),1);
        scatter3(setP(:,1),setP(:,2),setP(:,3),20,c,mkr{i});
        h{i} = scatter3(nan,nan,nan,20,'k',mkr{i});
    end
    legend([h{:}],"eps = " + string(num2str(epsArray(:))));
    xlabel('g1')
    ylabel('g2')
    zlabel('g3')
    grid on
    view(60,30)

    %% PLOT ALL COLOR BY PRICE
    clear 
    load('MILP_PRICE_CHANGES_EPS_0_9')

    % only G1-3 changes ,G4-5 fixed at zero

    % x axis show the sum of cost function
    % y axis show the sum of generator dispatch

    baseMVA = 100;
    genRange = [60 60 25 25];
    lenEps = length(epsArray);

    f = figure('Name','changing epsilon ** color by costFunc');
    ax = axes;
    hold on
    mkr = {'o','s','^','d'};
    h = cell(4,1);
    for i=1:lenEps
        eps = epsArray(i);
        genSetPoints = [pslack.*baseMVA,gsetpoints.*genRange];

        % use only every n th value AND filter out non stable points
        stabIdx = stability(i:lenEps:end,:);
        stabIdx = boolean(stabIdx);
        setP = genSetPoints(i:lenEps:end,:);

        colSumP= sum(setP(stabIdx),2);
        colSumCost = sum(spaceGrid,2);

    %     c = zeros(length(stabIdx),3);
    %     c(stabIdx,:) = repmat([0 1 0],sum(stabIdx),1);
    %     c(~stabIdx,:) = repmat([1 0 0],sum(~stabIdx),1);
        stab = scatter3(setP(stabIdx,1),setP(stabIdx,2),setP(stabIdx,3),40,colSumCost(stabIdx),mkr{i},'filled');
        stab.MarkerEdgeColor = [0 1 0];
        nonStab = scatter3(setP(~stabIdx,1),setP(~stabIdx,2),setP(~stabIdx,3),40,colSumCost(~stabIdx),mkr{i},'filled');
        nonStab.MarkerEdgeColor = [1 0 0];
        h{i} = scatter3(nan,nan,nan,20,'k',mkr{i});
    end
    title('Effects of varying cost function and varying \epsilon')
    l = legend([h{:}],"\epsilon = " + string(num2str(epsArray(:))),'location','southoutside','autoupdate','off','Orientation','horizontal');
    c = colorbar;
    c.Location = 'west';
    c.Position = [0.2089    0.4093    0.0389    0.4098];
    c.Label.String = 'Cost function sum';

    [GCount,GRoup]=groupcounts(colSumCost);
    xlabel('g1')
    ylabel('g2')
    zlabel('g3')
    grid on
    view(60,30)
end

%% PLOT ALL COLOR BY PRICE
clear 
load('MILP_PRICE_CHANGES_QS3_04_03_20T21_05_23')

% only G1-3 changes ,G4-5 fixed at zero

% x axis show the sum of cost function
% y axis show the sum of generator dispatch

baseMVA = 100;
genRange = [60 60 25 25];
lenEps = length(epsArray);

f = figure('Name','changing epsilon ** color by costFunc');
ax = axes;
hold on
% mkr = {'o','s','^','d'};
% h = cell(4,1);
for i=1:lenEps
    eps = epsArray(i);
    genSetPoints = [pslack.*baseMVA,gsetpoints.*genRange];

    % use only every n th value AND filter out non stable points
    stabIdx = stability(i:lenEps:end,:);
    stabIdx = boolean(stabIdx);
    setP = genSetPoints(i:lenEps:end,:);
    
    colSumP= sum(setP(stabIdx),2);
    colSumCost = sum(spaceGrid,2);


    stab = scatter3(setP(stabIdx,1),setP(stabIdx,2),setP(stabIdx,3),40,colSumCost(stabIdx),'filled');
    stab.MarkerEdgeColor = [0 1 0];
    nonStab = scatter3(setP(~stabIdx,1),setP(~stabIdx,2),setP(~stabIdx,3),40,colSumCost(~stabIdx),'filled');
    nonStab.MarkerEdgeColor = [1 0 0];
%     h{i} = scatter3(nan,nan,nan,20,'k',mkr{i});
end
title('Effects of varying cost function and varying \epsilon')
% l = legend([h{:}],"\epsilon = " + string(num2str(epsArray(:))),'location','southoutside','autoupdate','off','Orientation','horizontal');
c = colorbar;
c.Location = 'west';
c.Position = [0.2089    0.4093    0.0389    0.4098];
c.Label.String = 'Cost function sum';

[GCount,GRoup]=groupcounts(colSumCost);
xlabel('g1')
ylabel('g2')
zlabel('g3')
grid on
view(60,30)
