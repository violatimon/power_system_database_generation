f = figure;
ax = axes;
c = 1:3;%categorical({'ACOPF','N-1 ACOPF','MILP'});
set(ax,'xTickLabel',{'ACOPF','N-1 ACOPF','MILP'})
b = bar(c(1),1311.59);
hold on;
b1 = bar(c(2),1707.93);
b2 = bar(c(3),2093.00);

ax.Box = 'off';
ax.YGrid = 'on';
ax.XTickLabelRotation = 15;
title('Cost increase due to security')
ylabel('$/MW')
set(ax,'xTickLabel',{'ACOPF','N-1 ACOPF','MILP'})
ax.XTick = 1:3;