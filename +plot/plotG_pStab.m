% rose plot of G1 - G4
%% Load data from price changing .mat
%G = [pslack gsetpoints(:,[1 2 4])];
%% Load data from single MILP solution
G = [value(Pslack), value(u_NN')];
stability = 1; % set manually
%%
% % G = [1.0438
% % 0.9800
% % 0.9734
% % 0.6322];
% % G2 = [1.1438
% % 0.7800
% % 0.2734
% % 0.8322];

% Axes and figure settings
fh = figure('name','Stability wrt generator output');

ax = axes('PlotBoxAspectRatio',[1 1 1]);
ax.XLim = [-1.5 1.5];
ax.YLim = [-1.5 1.5]; 
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
ax.Box='on';
ax.XAxis.MinorTick = 'on';
ax.YAxis.MinorTick = 'on';
ax.XAxis.TickLabels{2} = 1;
ax.XAxis.TickLabels{3} = 0.5;
ax.YAxis.TickLabels{2} = 1;
ax.YAxis.TickLabels{3} = 0.5;
hold on

ch = circ(ax,0,0,1,{'FaceColor','none',...
    'EdgeAlpha',0.5,'LineStyle',':'});
labels(ax)
% mapping of axis:
%   G1 - x+
%   G2 - y+
%   G3 - x-
%   G5 - y-
% G4 can be added, to plot shall become a pentagon

% stable op
S = {'FaceColor','none',...%[0.4660 1 0.1880],...
    'FaceAlpha',0.2, 'EdgeColor',[0.4660 0.8740 0.1880]};

% unstable op
U = {'FaceColor','none',...%[1 0 0],...
    'FaceAlpha',0.1, 'EdgeColor',[0.8350 0.0780 0.1840]};

% =====
% Plot data

%gen(ax,G,S); stable
%gen(ax,G2,U); non-stable
for i=1:size(G,1)
    if stability(i)
        gen(ax,G(i,:),S);
    else
        gen(ax,G(i,:),U);
    end
end
% =====
function labels(ax)
    text(ax, 1.2,0.1,'G_1\rightarrow')
    text(ax, -1.45,0.1,'\leftarrowG_3')
    text(ax, 0.05,1.35,'\uparrow G_2')
    text(ax, 0.05,-1.35,'\downarrow G_5')
    
end

function varargout = gen(ax,G,props)
    vertices = [G(1) 0; 0 G(2); -1*G(3) 0; 0 -1*G(4)];
    f = [1 2 3 4];
    p = patch(ax,'Faces',f,'Vertices',vertices,props{:});
    varargout{1} = p;
end

function varargout = circ(ax,xb,yb,r,props)
    t = linspace(0, 2*pi);
    x = r*cos(t) + xb;
    y = r*sin(t) + yb;
    ph = patch(ax, x, y, 'g', props{:});
    varargout{1} = ph;
end

