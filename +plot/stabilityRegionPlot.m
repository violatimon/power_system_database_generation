% this plot will try to mimic the one in the paper draft
% Since there are 5 generatores in the 14 bus case G4 - G5 (bus6,8) will be
% fixed to 0 P output.

% In order to do this plot I have to recreate the dataset, since Pslack was
% never saved - or was it? as the Pdemand - sum(setpoints) == Pslack ?

%% ===== Load the input data (NN input is the same)
%util.unify_datasets
util.unify_q_s3_relaxed_dataset
%% ===== Data reduction
% The data-set has to be reduced, we only the the points where G4,5 are max
% that in the 'setpoints(:,[3 4]) == 25
%k = 25;
k = 20;
%pIdx = (setpoints(:,3) == k & setpoints(:,4) == k);
pIdx = (setpoints(:,4) >= k);
setpointsR = setpoints(pIdx, 1:3);
classificationR = classification(pIdx);
classDetailsR = classDetails(pIdx);

%% ===== plot
% plot the feasible region
fIdx = (classificationR == 1);

f = figure;
ax = axes;

h = scatter3(setpointsR(fIdx,1),setpointsR(fIdx,2),setpointsR(fIdx,3),'k');
h.MarkerFaceColor = 'g';
h.SizeData = 15;

ax.XLabel.String = 'P_{G1}';
ax.YLabel.String = 'P_{G2}';
ax.ZLabel.String = 'P_{G3}';
hold on
%% unstable
fIdx = (classificationR == 0);

h = scatter3(ax,setpointsR(fIdx,1),setpointsR(fIdx,2),setpointsR(fIdx,3),'k');
h.MarkerFaceColor = 'r';
h.SizeData = 5;

ax.XLabel.String = 'P_{G1}';
ax.YLabel.String = 'P_{G2}';
ax.ZLabel.String = 'P_{G3}';
l = legend('stable','unstable','Location','northeast','Box','off');
% az=130, el=45
%% Subplot fixing different axes
nG = size(setpoints,2);
k = max(setpoints) .* 0.8;

sf = figure('name','dataset subplots');
for i=1:nG
    pIdx = (setpoints(:,i) >= k(i));
    setpointsR = setpoints(pIdx,:);
    setpointsR(:,i) = [];
    classificationR = classification(pIdx);
    classDetailsR = classDetails(pIdx);
    ax = subplot(2,2,i);
    addSubplot(ax,setpointsR,classificationR,i,k(i))
end
l = legend('stable','unstable','Position',[0.8234 0.9090 0.1719 0.0793],'Box','on');

%% Convex hull of stable points
x = setpointsR(~fIdx,1);
y = setpointsR(~fIdx,2);
z = setpointsR(~fIdx,3);

% [k1,av1] = convhull(x,y,z);
%
% ht = trisurf(k1,x,y,z,'FaceColor','g','FaceAlpha',0.1);
%% It works better with boundary
k = boundary([x,y,z],1);
figure
ax = axes;
hh = trisurf(k,x,y,z,'DisplayName','Feasible region');
hh.EdgeColor = [.1 .4 .1];
hh.EdgeAlpha = .1;
hh.FaceColor = [0.2 0.9 .2];
hh.FaceAlpha = 0.5;
ax.XLabel.String = 'P_{G1}';
ax.YLabel.String = 'P_{G2}';
ax.ZLabel.String = 'P_{G3}';
pause(1)
oldAx = ax;
figure
ax = axes;
hold on
nhh = copyobj(hh,ax);
h = scatter3(ax,setpointsR(fIdx,1),setpointsR(fIdx,2),setpointsR(fIdx,3),'k',...
    'DisplayName','Unstable setpoints');
h.MarkerFaceColor = 'r';
h.SizeData = 5;
hold on
ax.XLabel.String = 'P_{G1}';
ax.YLabel.String = 'P_{G2}';
ax.ZLabel.String = 'P_{G3}';
title('Data-set stability region')


L={[32.4491   63.9167],[39.4077   70.8753],[12.4187   25.5302]};
set(ax,{'xlim','ylim','zlim'},L)

b = [49,130];
for i = b(1):b(2)
    view(i,15)
    pause(.05)
end

%% if there is solution
% run the milp
%MILP_with_losses_pslack
MILP_with_recur_losses
props = {'Marker','o','MarkerSize',5,'MarkerFaceColor','#FF6347','MarkerEdgeColor','k','LineStyle','none','DisplayName','MILP solution'};
p = plot3(setpoints(1),setpoints(2),setpoints(3),props{:});

props = {'LineStyle','--','LineWidth',2,'Color',[1 .4 .6]};
lX= line([ax.YLim(1)-10,ax.YLim(2)+10], [p.YData,p.YData], [p.ZData,p.ZData],props{:});
lY= line([p.XData,p.XData],[ax.YLim(1)-10,ax.YLim(2)+10], [p.ZData,p.ZData],props{:});
lZ= line([p.XData,p.XData],[p.YData,p.YData], [ax.ZLim(1)-10,ax.ZLim(2)+20],props{:});
l = legend({'Feasible Region';'Un-stable set points';'MILP solution'});
l.Location = 'northeastoutside';
l.Box='on';
l.Position = [0.5666    0.1945    0.2877    0.0841];
%%
copyobj(p,oldAx);
copyobj(lX,oldAx);
copyobj(lY,oldAx);
copyobj(lZ,oldAx);
%%

function addSubplot(ax,setpointsR,classificationR,currGen,k)
    % plot the feasible region
    fIdx = (classificationR == 1);
    
    h = scatter3(setpointsR(fIdx,1),setpointsR(fIdx,2),setpointsR(fIdx,3),...
        'MarkerEdgeColor','g',...
        'MarkerFaceColor','g');
    %h.MarkerFaceColor = 'g';
    %h.MarkerFaceAlpha = 0.8;
    h.SizeData = 8;
    genLabels = [1 2 3 4];
    genLabels(genLabels == currGen) = [];
    ax.XLabel.String = "P_{G"+genLabels(1)+"}";
    ax.YLabel.String = "P_{G"+genLabels(2)+"}";
    ax.ZLabel.String = "P_{G"+genLabels(3)+"}";
    hold on
    % unstable
    fIdx = (classificationR == 0);
    
    h = scatter3(ax,setpointsR(fIdx,1),setpointsR(fIdx,2),setpointsR(fIdx,3),...
        'MarkerEdgeColor','r',...
        'MarkerFaceColor','r');
    %h.MarkerFaceAlpha = 0.4;
    h.SizeData = 5;
%     
%     
    title({"fixed G" + currGen + ">="+ k;
        "N = " + length(setpointsR)});
    view(74,60)
    %l = legend('stable','unstable','Location','northeast','Box','off');
end
