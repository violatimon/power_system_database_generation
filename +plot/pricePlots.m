%load('matlabMILP3.mat')
%load('case14priceChangesPslack_qLims0__02_24_0102.mat')
%%
if exist('epsArray','var')
    sel = 3;
    fprintf('Results for eps = %2.2f\n',epsArray(sel))
    lenEps = size(epsArray,2);
    milpSolution.price = price(sel:lenEps:end);
    milpSolution.stability = stability(sel:lenEps:end);
else
    milpSolution.price = price;
    milpSolution.stability = stability;
end

%% scatter plot
a = round(milpSolution.price(:)./10);
a(a == 0) = nan;

l = length(a);

f = figure;
ax = axes;
h = scatter3(spaceGrid(1:l,1),spaceGrid(1:l,2),spaceGrid(1:l,3),a,a,'filled');

xlim([0,20])
ylim([0,20])
zlim([0,20])

ax.XLabel.String = 'Cost_{G1}';
ax.YLabel.String = 'Cost_{G2}';
ax.ZLabel.String = 'Cost_{G3}';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';
ax.ZMinorGrid = 'on';
ax.Title.String = {'Price variation';'G_6: 0 $/MW';'G_8: 0 $/MW'};
ax.Title.Position = [0.9049    7.6363    3.8887];
%colorbar;
view(50,50)
%%
X = spaceGrid(1:l,1);
Y = spaceGrid(1:l,2);
Z = spaceGrid(1:l,3);
V = milpSolution.price(:);

%%
warning('off')
f2 = figure('Position',[64 240 1049 410]);
sp1 = subplot(1,3,1);
trisurf(delaunay(X,Y),X,Y,V)
sp1.XLabel.String = 'C_{G1}';
sp1.YLabel.String = 'C_{G2}';
title('G1 vs G2')

sp2 = subplot(1,3,2);
trisurf(delaunay(X,Z),X,Z,V)
sp2.XLabel.String = 'C_{G1}';
sp2.YLabel.String = 'C_{G3}';
title('G1 vs G3')

sp3 = subplot(1,3,3);
trisurf(delaunay(Y,Z),Y,Z,V)
sp3.XLabel.String = 'C_{G2}';
sp3.YLabel.String = 'C_{G3}';
title('G2 vs G3')

for i = 0:90
   sp1.View = [i i];
   sp2.View = [i i];
   sp3.View = [i i]; 
   pause(0.01)
end
warning('on')

%% Analysis on price changes effect on stability
fprintf('Ratio of stable points: %3.2f%%\n',(sum(milpSolution.stability)/length(milpSolution.stability))*100)

%% stability plot

f = figure;
ax = axes;
st = boolean(milpSolution.stability);
hs = scatter3(spaceGrid(st,1),spaceGrid(st,2),spaceGrid(st,3),5,'g','filled',...
    'DisplayName','stable');
hold on
hu = scatter3(spaceGrid(~st,1),spaceGrid(~st,2),spaceGrid(~st,3),10,'r',...
    'DisplayName','unstable');
hu.Marker = 'd';
hu.SizeData = 15;
hu.MarkerEdgeColor = 'k';
hu.MarkerFaceColor = 'r';
xlim([0,20])
ylim([0,20])
zlim([0,20])

ax.XLabel.String = 'Cost_{G1}';
ax.YLabel.String = 'Cost_{G2}';
ax.ZLabel.String = 'Cost_{G3}';
% ax.XMinorGrid = 'on';
% ax.YMinorGrid = 'on';
% ax.ZMinorGrid = 'on';
l = legend;
l.Position = [0.6306    0.8524    0.1839    0.0821];
l.FontSize = 12;
view(22,42)
%% stability plot 2
f = figure;
ax = axes;
st = boolean(milpSolution.stability);
hs = scatter3(spaceGrid(st,1),spaceGrid(st,2),spaceGrid(st,3),5,'g','filled',...
    'DisplayName','stable');
hold on
hu = scatter3(spaceGrid(~st,1),spaceGrid(~st,2),spaceGrid(~st,3),10,'r',...
    'DisplayName','unstable');
hu.Marker = 'd';
hu.SizeData = 15;
hu.MarkerEdgeColor = 'k';
hu.MarkerFaceColor = 'r';

k = boundary(spaceGrid(~st,[1,2,3]),0);
hh = trisurf(k,spaceGrid(~st,1),spaceGrid(~st,2),spaceGrid(~st,3),'DisplayName','Unstable region');
hh.EdgeColor = [.4 .1 .1];
hh.EdgeAlpha = .1;
hh.FaceColor = [0.9 0.2 .2];
hh.FaceAlpha = 0.5;

xlim([0,20])
ylim([0,20])
zlim([0,20])

ax.XLabel.String = 'Cost_{G1}';
ax.YLabel.String = 'Cost_{G2}';
ax.ZLabel.String = 'Cost_{G3}';

l = legend;
l.Position = [0.6306    0.8524    0.1839    0.0821];
l.ItemHitFcn = @legendCallback;


%%
%=========== try to plot by slices

f = figure;
ax = axes;
st = boolean(milpSolution.stability);
hs = scatter3(spaceGrid(st,1),spaceGrid(st,2),spaceGrid(st,3),5,'g','filled',...
    'DisplayName','stable');
hold on
hu = scatter3(spaceGrid(~st,1),spaceGrid(~st,2),spaceGrid(~st,3),10,'r',...
    'DisplayName','unstable');
hu.Marker = 'd';
hu.SizeData = 15;
hu.MarkerEdgeColor = 'k';
hu.MarkerFaceColor = 'r';

% Sections (according to G1 prices)
sects = unique(spaceGrid(:,1));
props = {'Color','r'};
% props = {'EdgeColor', [.4 .1 .1],...
%     'EdgeAlpha', .1,...
%     'FaceColor',[0.9 0.2 .2],...
%     'FaceAlpha', 0.5};

for i = 1:length(sects)
    pointsIdx = ~st & spaceGrid(:,1) == sects(i);
    if sum(pointsIdx) > 1
        try
            P = spaceGrid(pointsIdx,[2,3]);

            if (sum(P(:,2) == P(1,2)) == length(P(:,2))) || (sum(P(:,1) == P(1,1)) == length(P(:,1)))
               error('MYERR:COLINEARPOINTS','Could not compute convex hull because points are co-linear') 
            end
            [k,av] = convhull(P);
            plot3(repmat(sects(i),length(P(k,1)),1),P(k,1),P(k,2),'Color','r',...
                'DisplayName',"section " + round(sects(i)));
        catch ME
            if (strcmp(ME.identifier,'MYERR:COLINEARPOINTS'))
                continue
            else
                rethrow(ME)
            end
        end
    end
end

x = spaceGrid(~st,1);
y = spaceGrid(~st,2);
z = spaceGrid(~st,3);
[k1, av1] = convhull(x,y,z);

trt = trisurf(k1, x,y,z,'FaceColor','r');
trt.FaceAlpha = .1;
trt.EdgeColor = 'none';
trt.DisplayName = 'Un-stable region';

xlim([0,20])
ylim([0,20])
zlim([0,20])

ax.XLabel.String = 'Cost_{G1}';
ax.YLabel.String = 'Cost_{G2}';
ax.ZLabel.String = 'Cost_{G3}';

l = legend;
l.ItemHitFcn = @legendCallback;
l.Position = [0.6592 0.5384 0.2649 0.3866];
view(164,66)

% plot g setpoints vs price sum --> in plotEpsChanges.m


function legendCallback(src,evnt)
% Toggle legend item visibility
if strcmp(evnt.Peer.Visible,'on')
    evnt.Peer.Visible = 'off';
else
    evnt.Peer.Visible = 'on';
end 
end