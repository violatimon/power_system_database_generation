if exist('epsArray','var')
    sel = 1;
    fprintf('Results for eps = %2.2f\n',epsArray(sel))
    lenEps = size(epsArray,2);
    milpSolution.dr = dampingRatio(sel:lenEps:end);
else
    milpSolution.dr = dampingRatio;
end

%% scatter plot
a = round(milpSolution.dr(:).*1000);
a(a == 0) = nan;

l = length(a);

f = figure;
ax = axes;
h = scatter3(spaceGrid(1:l,1),spaceGrid(1:l,2),spaceGrid(1:l,3),a,a,'filled');

xlim([0,20])
ylim([0,20])
zlim([0,20])

ax.XLabel.String = 'Cost_{G1}';
ax.YLabel.String = 'Cost_{G2}';
ax.ZLabel.String = 'Cost_{G3}';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';
ax.ZMinorGrid = 'on';
ax.Title.String = {'Price variation';'G_6: 0 $/MW';'G_8: 0 $/MW'};
ax.Title.Position = [2.8616   27.1992    8.8610];
%colorbar;
% dataTip
drValue = string(num2str(round(milpSolution.dr*100,2))) + ' %';
h.DataTipTemplate.DataTipRows(end+1) = dataTipTextRow('DR',drValue);

view(-25,75)
%%
X = spaceGrid(1:l,1);
Y = spaceGrid(1:l,2);
Z = spaceGrid(1:l,3);
V = milpSolution.dr(:);

%%
warning('off')
f2 = figure;
sp1 = subplot(1,3,1);
trisurf(delaunay(X,Y),X,Y,V)
sp1.XLabel.String = 'C_{G1}';
sp1.YLabel.String = 'C_{G2}';
title('G1 vs G2')

sp2 = subplot(1,3,2);
trisurf(delaunay(X,Z),X,Z,V)
sp2.XLabel.String = 'C_{G1}';
sp2.YLabel.String = 'C_{G3}';
title('G1 vs G3')

sp3 = subplot(1,3,3);
trisurf(delaunay(Y,Z),Y,Z,V)
sp3.XLabel.String = 'C_{G2}';
sp3.YLabel.String = 'C_{G3}';
title('G2 vs G3')

% for i = 0:90
%    sp1.View = [i i];
%    sp2.View = [i i];
%    sp3.View = [i i]; 
%    pause(0.01)
% end
warning('on')