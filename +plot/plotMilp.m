
% Load data set that contains the database points
load('priceChangesPslackIncl_220120.mat')
% ====== Plot the data set
f = figure;
ax = axes;
st = boolean(stability);
hs = scatter3(spaceGrid(st,1),spaceGrid(st,2),spaceGrid(st,3),5,'g','filled',...
    'DisplayName','stable');
hold on
hu = scatter3(spaceGrid(~st,1),spaceGrid(~st,2),spaceGrid(~st,3),10,'r',...
    'DisplayName','unstable');
hu.Marker = 'd';
hu.SizeData = 15;
hu.MarkerEdgeColor = 'k';
hu.MarkerFaceColor = 'r';

% Sections (according to G1 prices)
sects = unique(spaceGrid(:,1));
props = {'Color','r'};
% props = {'EdgeColor', [.4 .1 .1],...
%     'EdgeAlpha', .1,...
%     'FaceColor',[0.9 0.2 .2],...
%     'FaceAlpha', 0.5};

for i = 1:length(sects)
    pointsIdx = ~st & spaceGrid(:,1) == sects(i);
    if sum(pointsIdx) > 1
    P = spaceGrid(pointsIdx,[2,3]);
    [k,av] = convhull(P);
    plot3(repmat(sects(i),length(P(k,1)),1),P(k,1),P(k,2),'Color','r',...
        'DisplayName',"section " + round(sects(i)));
    end
end

x = spaceGrid(~st,1);
y = spaceGrid(~st,2);
z = spaceGrid(~st,3);
[k1, av1] = convhull(x,y,z);

trt = trisurf(k1, x,y,z,'FaceColor','r');
trt.FaceAlpha = .1;
trt.EdgeColor = 'none';
trt.DisplayName = 'Un-stable region';

xlim([0,20])
ylim([0,20])
zlim([0,20])

ax.XLabel.String = 'Cost_{G1}';
ax.YLabel.String = 'Cost_{G2}';
ax.ZLabel.String = 'Cost_{G3}';

% l = legend;
% l.ItemHitFcn = @legendCallback;
% l.Position = [0.6592 0.5384 0.2649 0.3866];
% view(164,66)

%===== have to use setpoints(1:3)



function legendCallback(src,evnt)
% Toggle legend item visibility
if strcmp(evnt.Peer.Visible,'on')
    evnt.Peer.Visible = 'off';
else
    evnt.Peer.Visible = 'on';
end 
end