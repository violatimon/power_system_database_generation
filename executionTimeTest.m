%fName = 'log/MILP_PRICE_CHANGES_QS3_25_02_20T14_34_52.log';
fName = 'log/MILP_PRICE_CHANGES_QS3_29_02_20T17_59_16.log';
fileID = fopen(fName,'r');
fSpec = '%s'; % as string
in = fscanf(fileID,fSpec); % apparently matlab cannot match regexp in file read...
fclose(fileID);

regExpre = '(?<=timeis)[0-9]*.[0-9]*'; % positive look behind assertion in order to extract the time stamp values
[C,matches] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
% process matches into numeric
timeStamps = str2double(matches);

% assume every timestamp entry belongs to the same operation => I can
% simply average
%%
f = figure('Position',[403   246   560   256]);
ax = axes;
h = plot(1:length(timeStamps),timeStamps,'k','LineWidth',1.5);
title({'Execution time of';fName},'interpreter','none')
%ax.Position = [0.1300    0.1100    0.7750    0.7277];
ax.YGrid = 'on';
%ax.YMinorGrid = 'on';
xlabel('Time elapsed [s]')
ylabel('Iteration count')

%%
timeEtaps = diff(timeStamps(2:end));
avgTime = mean(timeEtaps);
f = figure('Position',[403   246   560   256]);
ax = axes;
h = plot(1:length(timeEtaps),timeEtaps,'k','LineWidth',1.5);
l = line(ax.XLim,[avgTime,avgTime],'DisplayName',"Average execution time: " + num2str(avgTime,'%2.2f') + " s",'color','r','LineStyle',':','LineWidth',2);
title({'Execution time of';fName},'interpreter','none')
ax.YGrid = 'on';
legend(l,'Location','northwest','Box','off')
xlabel('Iteration count')
ylabel('Execution time [s]')


