
fName = 'log/MILP_PRICE_CHANGES_QS3_04_03_20T21_05_23.log';
fileID = fopen(fName,'r');
fSpec = '%s'; % as string
in = fscanf(fileID,fSpec); % apparently matlab cannot match regexp in file read...
fclose(fileID);

% Skip initial toc and final toc
in = in(250:end-50);

regExpre = '(?<=timeis)[0-9]*.[0-9]*'; % positive look behind assertion in order to extract the time stamp values
[C,matches] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
% process matches into numeric
timeStamps = str2double(matches);

% assume every timestamp entry belongs to the same operation => I can
% simply average


%% Get number or [0] iteration errors
regExpre = '\[0\]'; % positive look behind assertion in order to extract the time stamp values
[Ce0,matches_e0] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
err_0 = str2double(matches_e0);

regExpre = '\[1\]'; % positive look behind assertion in order to extract the time stamp values
[Ce1,matches_e1] = strsplit(in,regExpre,'DelimiterType','RegularExpression');
err_1 = str2double(matches_e1);
%%
fprintf('Ratio of 2nd error estimation iteration: %3.2f %%\n',(size(matches_e1,2)/size(matches_e0,2))*100)
fprintf('Average loss estimation time: %3.2f sec\n',mean(timeStamps)) % averaging on all iterations
fprintf('Average solution time: %3.2f sec\n',sum(timeStamps)/size(matches_e0,2)) % accounting for iterations
fprintf('Average MILP time: %3.2f sec\n',mean(timeStamps(1:11:end))) % eps 0
nTimeStamps = timeStamps;
nTimeStamps(1:11:end) = [];
fprintf('Average MILP+eps time: %3.2f sec\n',mean(nTimeStamps))



