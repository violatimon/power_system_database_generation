% Generate database w/ generator setpoints and classification of o.p.
% Output: classification - the columns are: stable, ac_opf stable, SSS
%         classDetails  - detailed struct of N-1 operating point stability
%         setPoints     - Generator 2-5 setpoints
%
% Note: all caps variable names are cleared at the end of the script
%% ----- Set up -----
clear all
close all
tic
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, RATE_C, TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST,  ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;

% load case
mpcInit = util.case14_wind;

% matpower options - surpress output of runpf
mpOption = mpoption;
mpOption.verbose = 0;
mpOption.out.all = 0;

qOpt = 0;
mpOption.pf.enforce_q_lims = qOpt;
% 0 - do not enforce -> no feasable point, Q never satisfied in all N-1
% scenarios
% 1 - enforce limits, simultaneous -> ?
% 2 - enforce, one at a time
%

genList = util.GetGenList(mpcInit);% generator indeces without slack
%% relax q ,s limits
mpcInit.gen(:,QMAX) = mpcInit.gen(:,QMAX)+0.25*(mpcInit.gen(:,QMAX)-mpcInit.gen(:,QMIN));
mpcInit.gen(:,QMIN) = mpcInit.gen(:,QMIN)-0.25*(mpcInit.gen(:,QMAX)-mpcInit.gen(:,QMIN));

sTol = 1.3;
mpcInit.branch(:,6) = mpcInit.branch(:,6)*sTol; 
mpcInit.branch(:,7) = mpcInit.branch(:,7)*sTol;
mpcInit.branch(:,8) = mpcInit.branch(:,7)*sTol;
% %% ------ In order to relax the N-1 burden the most critical contingencies are removed
% % based on 'Contingency failure' figure of util.test_dataset the following
% % contingencies are removed ([13    14] were originally removed):
% %   [1    2    10    12    13    14]
% %mpcInit.contingencies = [3     4     5     6     7     8     9    11    15    16    17    18    19    20];
% mpcInit.contingencies = [1    2    3     4     5     6     7     8     9    10    11    15    16    17    18    19    20];
%%
cont = mpcInit.contingencies; % list of contingencies to be checked
nb = length(mpcInit.branch(:,1));

mpc = mpcInit;
warning('off', 'all') % surpress checkOP warnings (singular matrix)
%% ----- setpoints -----
% steps of generator operating points
% step = input('\nDefine step size (default 1): ');
% if isempty(step)
%     step = 1;
%     fprintf('Continue with default step size %d\n',step);
% %elseif(floor(step) ~= step)
% %    fprintf('Error. Step size must be an integer.\n');
% %    return
% elseif((step > 10) || (step < 1))
%     fprintf('Error. Step size must be in [1, 10].\n')
%     return
% end


PMX = mpc.gen(2:5,PMAX); %case14db_qLims0_q_s_Relaxed_02_25_1230.mat case14db_qLims0_qRelaxed_02_20_0011 %4th run case14db_qLims0_qRelaxed_02_20_1200 case14db_qLims0_qRelaxed_02_20_1229 case14db_qLims0_qRelaxed_02_20_1259 case14db_qLims0_qRelaxed_02_20_1335 case14db_qLims0_qRelaxed_02_20_1441 qRelaxed_02_20_1507 case14db_qLims0_q_s_Relaxed_02_25_0901  case14db_qLims0_q_s_Relaxed_02_25_0933.mat case14db_qLims0_q_s_Relaxed_02_25_0946 case14db_qLims0_q_s_Relaxed_02_25_1136.mat case14db_qLims0_q_s_Relaxed_02_25_1154.mat case14db_qLims0_q_s_Relaxed_02_25_1206.mat
%PMX = 0.9*mpc.gen(2:5,PMAX); 2nd run case14db_qLims0_qRelaxed_02_19_2212
%PMN = [48; 48; 12.5; 12.5]; %4th run
%PMN = mpc.gen(2:5,PMIN); % 3rd run case14db_qLims0_qRelaxed_02_20_0011 qRelaxed_02_20_1507 case14db_qLims0_q_s_Relaxed_02_25_0946 case14db_qLims0_q_s_Relaxed_02_25_1206.mat
%PMN = 0.7 * PMX; 2nd run
%PMN = [52; 52; 20; 20];
%PMN = [55;55;20;20];% case14db_qLims0_qRelaxed_02_20_1200, case14db_qLims0_qRelaxed_02_20_1229 case14db_qLims0_q_s_Relaxed_02_25_0933.mat case14db_qLims0_q_s_Relaxed_02_25_1154.mat
%PMN = [55;55;20;24];% case14db_qLims0_qRelaxed_02_20_1259
%PMN = [54;54;24.2;24]; %case14db_qLims0_qRelaxed_02_20_1335 case14db_qLims0_q_s_Relaxed_02_25_1136.mat
PMN = [53;53;23;23]; %case14db_qLims0_q_s_Relaxed_02_25_1230.mat case14db_qLims0_qRelaxed_02_20_1441 case14db_qLims0_q_s_Relaxed_02_25_0901
% no of points for equal spaced sampling
%gOp = [25 25 11 11];
%gOp = [30 30 7 7];% 2nd run case14db_qLims0_qRelaxed_02_19_2212  % 3rd run case14db_qLims0_qRelaxed_02_20_0011
%gOp = [12 12 25 25];% 4th run
%gOp = [10 10 10 10];% test case14db_qLims0_qRelaxed_02_20_1200 case14db_qLims0_qRelaxed_02_20_1229 case14db_qLims0_qRelaxed_02_20_1259 case14db_qLims0_qRelaxed_02_20_1335 case14db_qLims0_q_s_Relaxed_02_25_0933.mat case14db_qLims0_q_s_Relaxed_02_25_1136.mat case14db_qLims0_q_s_Relaxed_02_25_1154.mat
gOp = [14,14,8,8];% steps .5 .5 .25 .25 case14db_qLims0_q_s_Relaxed_02_25_1230.mat case14db_qLims0_qRelaxed_02_20_1441 case14db_qLims0_q_s_Relaxed_02_25_0901
%gOp = [12 12 5 5]; % 1507  case14db_qLims0_q_s_Relaxed_02_25_0946 case14db_qLims0_q_s_Relaxed_02_25_1206.mat

[p2,p3,p4,p5] = ndgrid(linspace(PMN(1),PMX(1),gOp(1)),...
                    linspace(PMN(2),PMX(2),gOp(2)),...
                    linspace(PMN(3),PMX(3),gOp(3)),...
                    linspace(PMN(4),PMX(4),gOp(4)));
setpoints = [p2(:) p3(:) p4(:) p5(:)];
fprintf('First set point:\n\tp2\tp3\tp4\tp5\n\t%d\t%d\t%d\t%d\n',p2(1),p3(1),p4(1),p5(1))

%% ----- initialize output -----
n = size(setpoints, 1);
fprintf('Checking %d setpoints.\n',n)
classification = nan(n,2); % [AC-OPF constraints, small sig stab]
classDetails = cell(n,1);
dampingRatio = nan(n,1);

%% ----- classification -----
fprintf(1,'\nClassifying setpoints.\n')

pw = PoolWaitbar(n, 'Classifying');
parfor iter = 1:n
%for iter = 1:n
    [classification(iter,:),classDetails{iter},dampingRatio(iter)] = util.classifyCase(mpc,setpoints(iter,:),genList,mpOption);
    increment(pw);
end
delete(pw);
%% ----- Output -----
fprintf('\nChecked %d cases.\n', n)
classification = [((classification(:,1) == 1) & (classification(:,2) == 1)), classification(:,1), classification(:,2)];
fileName = strcat('case14db','_qLims',num2str(qOpt),'_','q_s_Relaxed', datestr(now(),'_mm_dd_HHMM'),'.mat');
save(fileName,'classification', 'setpoints','classDetails','dampingRatio');
fprintf('Output file: %s\n',fileName)
% writematrix(setpoints,'NN_input.csv')
% writematrix(classification(:,2:3),'NN_output.csv')
%% ----- Tear down -----
warning('on', 'all') % surpress checkOP warnings
% get rid of matpower vars for easier debug
clear -regexp ^[^a-z]*$
toc

